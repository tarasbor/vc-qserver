#if defined(WINDOWS)
        #include <windows.h>

        #if !defined(AS_APP)
            #define AS_SERVICE
        #endif
#else
        #define MAX_PATH        255
#endif

#include <iostream>
#include <cstdlib>

#include <error.hpp>
#include <log.hpp>
#include <log_file_writer.hpp>
#include <time.hpp>
#include <system_env.hpp>
#include <net_server.h>
#include <net_lib_error.h>
#include <text_help.hpp>
#include <config_parser.hpp>
#include <net_cmd_client.hpp>

#include "../common/version.hpp"
#include "../common/user.hpp"
#include "../common/settings.hpp"
#include "../common/db_obj/server.hpp"

#include "net_control_cserver.hpp"
#include "net_data_cserver.hpp"
#include "gl_server.hpp"

#if defined(WINDOWS)
    HINSTANCE hAppInst;
    char CmdLine[MAX_PATH];

    #if defined(AS_SERVICE)
        SERVICE_STATUS glSS;
        SERVICE_STATUS_HANDLE glSS_H;
    #endif
#else
#endif

#ifdef VC_DB2
    #include <base_db2.hpp>
    #define CREATE_DB   create_db_db2()
#endif
#ifdef VC_ORACLE
    #include <base_oracle.hpp>
    #define CREATE_DB   create_db_oracle()
#endif


_net_server *control_server;
_net_server_processor *control_processor;

#define LPROG(A) _log::_set_module_level("prog",(A))
_log log;

class _config_file:public _config_parser_from_file
{
public:
	_config_file() {};
	virtual ~_config_file() {};

	virtual void parse_done() const;
};


#if defined(WINDOWS)
    int APIENTRY WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow);
    VOID WINAPI MainServiceStart(DWORD dwArgc,LPTSTR *lpszArgv);

    #if defined(AS_SERVICE)
        VOID WINAPI ServiceHandler(DWORD fdwControl);
    #endif
#else

    int main(int n,char *p[]);

#endif

int main_loop(int params_n,char *params[]);
int init();
int shutdown();

#if defined(WINDOWS)
// ----------------------------------------------------------
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
//    _server_status().state(_server_status::_state(-1,"������ ��������..."));

    hAppInst = hInstance;
    strncpy(CmdLine,lpCmdLine,MAX_PATH-1);

    // run only one copy
    SECURITY_ATTRIBUTES SA;
    SA.nLength=sizeof(SECURITY_ATTRIBUTES);
    SA.lpSecurityDescriptor=NULL;
    SA.bInheritHandle=TRUE;
    CreateMutex(&SA,TRUE,"_RDG_VIDEOCONSULTANT_QSERVER_");
    if (GetLastError()==ERROR_ALREADY_EXISTS) return 1;

    #ifndef AS_APP

    SERVICE_TABLE_ENTRY   DispatchTable[] =
    {
        { TEXT("RDG Video Consultant Q Server"), MainServiceStart},
        { NULL,NULL }
    };

    if (!StartServiceCtrlDispatcher( DispatchTable))
    {
        return GetLastError();
    }

    #else

    MainServiceStart(1,NULL);

    #endif

    return 0;
}

#ifndef AS_APP
// ----------------------------------------------------------
VOID WINAPI ServiceHandler(DWORD fdwControl)
{
    if (fdwControl==SERVICE_CONTROL_STOP || fdwControl==SERVICE_CONTROL_SHUTDOWN)
    {
        _server_status().need_exit(true);
    }
    else if (fdwControl==SERVICE_CONTROL_INTERROGATE)
    {
        SetServiceStatus(glSS_H,&glSS);
    }
    else if (fdwControl==SERVICE_CONTROL_CONTINUE || fdwControl==SERVICE_CONTROL_PAUSE)
    {
        //?????
    }
}
#endif

// ----------------------------------------------------------
VOID WINAPI MainServiceStart(DWORD dwArgc,LPTSTR *lpszArgv)
{
    #ifndef AS_APP
    glSS.dwServiceType   = SERVICE_WIN32_OWN_PROCESS;
    glSS.dwCurrentState  = SERVICE_START_PENDING;
    glSS.dwWin32ExitCode = 0;
    glSS.dwServiceSpecificExitCode = 0;
    glSS.dwCheckPoint    = 0;
    glSS.dwWaitHint      = 0;

    glSS_H = RegisterServiceCtrlHandler("RDG Video Consultant Q Server",ServiceHandler);
    if (!glSS_H)
    {
        return;
    }

    BOOL bres = SetServiceStatus(glSS_H,&glSS);
    if (!bres)
    {
        return;
    }
    #endif

    if (strlen(CmdLine))
    {
        _server_status().config_file_name(CmdLine);
    }
    else
    {
        _server_status().config_file_name(se_get_exe_path(hAppInst)+"vc_qserver.conf");
    }

    main_loop(0,NULL);

    return;
}

#else          // *nix

// ----------------------------------------------------------
int main(int n,char *p[])
{
    if (n==1) // no params
    {
        _server_status().config_file_name("/etc/vc/vc_qserver.conf");
    }
    else if (n==2)
    {
        _server_status().config_file_name(p[1]);
    }
    else
    {
        printf("qserver::too many params\nUsage: qserver [config_file_name]\n");
        return -1;
    }

    // TODO demonize

    return main_loop(n,p);
}

#endif

// ----------------------------------------------------------
int main_loop(int params_n,char *params[])
{
    bool first_init=true;
    _server_status().need_restart(true);

    while(1)
    {

        if (_server_status().need_exit()) break;
        if (_server_status().need_restart())
        {
            #if defined(WINDOWS) && defined(AS_SERVICE)
            glSS.dwCurrentState  = SERVICE_RUNNING;
            glSS.dwWin32ExitCode = 0;
            glSS.dwServiceSpecificExitCode = 0;
            glSS.dwCheckPoint    = 0;
            glSS.dwControlsAccepted  = 0;
            SetServiceStatus(glSS_H,&glSS);
            #endif

            int err;
            if (!first_init)
            {
                log << LPROG(0) << "I will restart...Wait 5 sec..." << endl;
                Sleep(5000);

                err = shutdown();
                if (err)
                {
                    #if defined(WINDOWS) && defined(AS_SERVICE)
                    glSS.dwCurrentState  = SERVICE_STOPPED;
                    glSS.dwWin32ExitCode = err;
                    glSS.dwServiceSpecificExitCode = err;
                    SetServiceStatus(glSS_H,&glSS);
                    #endif

                    return err;
                }
            }

            err = init();
            if (err)
            {
                #if defined(WINDOWS) && defined (AS_SERVICE)
                glSS.dwCurrentState  = SERVICE_STOPPED;
                glSS.dwWin32ExitCode = err;
                glSS.dwServiceSpecificExitCode = err;
                SetServiceStatus(glSS_H,&glSS);
                #endif

                return err;
            }

            first_init=false;
            _server_status().need_restart(false);

            #if defined(WINDOWS) && defined(AS_SERVICE)
            glSS.dwCurrentState  = SERVICE_RUNNING;
            glSS.dwWin32ExitCode = 0;
            glSS.dwServiceSpecificExitCode = 0;
            glSS.dwCheckPoint    = 0;
            glSS.dwControlsAccepted  = SERVICE_ACCEPT_STOP;
            SetServiceStatus(glSS_H,&glSS);
            #endif
        }

        Sleep(1000);

    }

    shutdown();

    #if defined(WINDOWS) && defined(AS_SERVICE)
    glSS.dwCurrentState  = SERVICE_STOPPED;
    glSS.dwWin32ExitCode = 0;
    glSS.dwServiceSpecificExitCode = 0;
    glSS.dwCheckPoint    = 0;
    SetServiceStatus(glSS_H,&glSS);
    #endif

    return 0;
}


// ----------------------------------------------------------
int init()
{
    try
    {
        _server_status().state(_server_status::_state(false));

        #ifdef WINDOWS
        log.set_writer(new _log_file_writer(se_get_exe_path(hAppInst) + "vc_qserver.log"));
        #else
        log.set_writer(new _log_file_writer("/var/log/vc/vc_qserver.log"));
        #endif

        log.max_level4module("prog", 15);

        log << LPROG(0) << "___________________________________________________________________________________________________" << endl;

        log << LPROG(0) << platform_name << " Q Server start... OK" << endl;
        log << LPROG(0) << "Version: " << version << " BDT: " << build_date << " " << build_time << endl;
//        log << "Flags are:";

        #ifdef AS_APP
        log << "Starting as application..." << endl;
        #else
        log << "Starting as service..." << endl;
        #endif

        #ifdef TCP_SERVER_TO_REINIT
        log << "TCP_SERVER_TO_REINIT = " << TCP_SERVER_TO_REINIT << endl;
        #else
        #endif

        log << "Start log...OK" << endl;

        // -------------------------------------------------------------
        log << LPROG(0) << "Loading local set... " << std::flush;
        _config_file *cf=new _config_file;
		cf->load(_server_status().config_file_name());
		delete cf;
        log << LPROG(0) << "OK" << endl;

        while(1)
        {
            try
            {
                log << LPROG(0) << "Trying connect to main server... " << std::flush;
                _server_status().net_control(create_simple_net_cmd_client());
                _server_status().net_control()->connect(_tcp_adress(_server_status().main_server_name()));
                log << LPROG(0) << "OK" << endl;
            }
            catch(_nerror &e)
            {
                log << LPROG(0) << "error " << e.err_st_full() << std::endl;
                log << LPROG(0) << "Wait 5 seconds & try again... " << std::endl;
                Sleep(5000);
                continue;
            }

            break;
        }

        log << LPROG(0) << "Checking main server status..." << std::flush;
        while(1)
        {
            std::string server_status;
            _server_status().net_control()->cmd("get_server_run_status", server_status);

//            if(server_status.compare("+ok: online;\r\n"))
            if(server_status[0] == '+') break;

            Sleep(1000);
        }
        log << LPROG(0) << "OK - main server is on." << endl;

        log << LPROG(0) << "Logging to main server..." << endl;
        std::string res;
        _server_status().net_control()->cmd("login qserver qserver", res);
        if(res[0] != '+')
            throw(_error(0,std::string("Can not login\n")+res.c_str()));
        _server_status().net_control()->cmd("get_global_settings", res);
        if(res[0] != '+')
            throw(_error(0,std::string("Can't retrieve settings from server ") + _server_status().main_server_name().c_str() + "\n" + res.c_str()));

        _settings::_global().load_from_text(res);

        // -------------------------------
        log << LPROG(0) << "Get local dns name & local q server settings..." << std::flush;
        _settings::_system::_q_servers::const_iterator qs = _settings::_global().get()->get_local_q_server();
        string self_dns_name = (*qs).second.dns_name;
        log << LPROG(0) << "DNS NAME = " << self_dns_name << endl;

        // NORMAL LOG LEVEL INIT
        int pn=0;
        while(1)
        {
        	string pv=_text_help::get_field_from_st((*qs).second.log_levels,";,",pn);
        	if (pv.empty()) break;

        	string par=_text_help::get_field_from_st(pv,':',0);
        	string val=_text_help::get_field_from_st(pv,':',1);

        	_log().max_level4module(par,atoi(val.c_str()));

        	pn++;
        }

        // OFF time sink - it is not good idea to do it so
        /*
        if(self_dns_name != _text_help::get_field_from_st(_server_status().main_server_name(), ':', 0))
        {
            log << LPROG(0) << "Syncing date and time..." << std::flush;
            _server_status().net_control()->cmd(_server_status().main_server_name(), "get_time", res);
            if(res[0] != '+')
                throw(_error(0, std::string("Can not get time from main server\n") + res.c_str()));

            std::string time_st;
            int pos = res.find("time = ");
            if(pos != std::string::npos)
            {
                pos += 7;
                int pos2=res.find(";", pos);
                time_st = std::string(res, pos, pos2 - pos);

                _time t;
                t.str_to_stamp(time_st.c_str());

                _time::_ys ys = t.split();

                SYSTEMTIME tm;
                tm.wYear = ys.y;
                tm.wMonth = ys.m;
                tm.wDay = ys.d;
                tm.wHour = ys.h;
                tm.wMinute = ys.n;
                tm.wSecond = ys.s;
                tm.wMilliseconds = ys.ms;

                if (!SetLocalTime(&tm)) throw(_error(GetLastError(),std::string("Can not set time")));

            }
            else throw(_error(0, std::string("Wrong answer from main server at get_time command\n") + res.c_str()));

            log << LPROG(0) << "OK" << endl;
        }
        */

        log << LPROG(0) << "Connecting database..." << std::flush;
        _server_status().database(CREATE_DB); //new _db((*hs).second.max_db_connections));
        int con_cnt = 0;
        bool error_connect = false;
        while(con_cnt < 3)
        {
            try
            {
                _server_status().database()->connect(_settings::_global().get()->db_info.dsn,
                                                      _settings::_global().get()->db_info.user,
                                                      _settings::_global().get()->db_info.pass);

                log << LPROG(0) << " get self DB id..." << std::flush;

                _transaction *tr;
                tr = _server_status().database()->transaction();
                tr->start();

                _db_cl::_qservers_list::_dns_name_eq qit_cond(self_dns_name);
                _db_cl::_qservers_list::iterator *qit = _db_cl::_qservers_list().begin(tr,&qit_cond);
                if(qit->eof())
                {
                    delete qit;
                    tr->commit();
                    delete tr;

                    throw(_error(0,"Can not get self DB id...check DB data..."));
                }

                _server_status().self_db_id((*qit)->id());
                delete qit;

                tr->commit();
                delete tr;
            }
            catch(_db_error &e)
            {
                log << LPROG(0) << "Connect to db failed: " << e.error_st() << endl << "Will try in 20sec." << endl;
                con_cnt++;
                Sleep(20000);
                error_connect=true;
            }
            if (!error_connect) break;
        }

        if(error_connect)
            throw(_error(0, "Database server is down or wrong db settings"));

        log << LPROG(0) << "OK" << endl;

        log << LPROG(0) << "Create & init storages..." << std::flush;
        _server_status().storages(create_storages());
        _server_status().storages()->init(_server_status().self_db_id());
        log << LPROG(0) << "Create & init storages...OK" << endl;

        log << LPROG(0) << "Start Call Station..." << std::flush;
        _server_status().call_station(create_vcall_station());//tmp_dir_cs));
        log << LPROG(0) << "Start Call Station...OK" << endl;

        log << LPROG(0) << "Start control server..." << std::flush;
        control_server = create_net_control_server();
        control_processor = create_net_control_processor();
        control_server->set_processor(control_processor);
        control_server->online(_tcp_adress((*qs).second.control_ip,(*qs).second.control_port));
        log << LPROG(0) << "Start control server...OK" << endl;

        log << LPROG(0) << "Start data server..." << std::flush;
        _server_status().data_server(create_net_data_server());
        _server_status().data_server()->online(_tcp_adress((*qs).second.data_ip,(*qs).second.data_port));
        log << LPROG(0) << "Start data server...OK" << endl;

        _server_status().state(_server_status::_state(true));
    }
    catch(_error &e)
    {
        log << LPROG(0) << "ERROR: " << e.str() << endl;
        #if defined(WINDOWS)
            #if defined(AS_APP)
                string caption(platform_name);
                caption += " ";
                caption += version;
                caption += " - qserver";
                MessageBox(NULL, e.str().c_str(), caption.c_str(), MB_OK);
            #endif
        #else
            printf("qserver::init error:\n%s\n",e.str().c_str());
        #endif

        return -1;
    }

    return 0;
}

// --------------------------------------------------
int shutdown()
{
    log << LPROG(0) << "Shutdown start..." << endl;
    _server_status().state(_server_status::_state(false));

    try
    {
        log << LPROG(0) << "Shutdown Call Station..." << endl;
        if (_server_status().call_station())
        {
            delete _server_status().call_station();
            _server_status().call_station(NULL);
        }
        log << LPROG(0) << "Shutdown Call Station...OK" << endl;

        log << LPROG(0) << "Shutdown control server..." << endl;
        control_server->shutdown();
        control_server->release();
        delete control_processor;
        log << LPROG(0) << "Shutdown control server...OK" << endl;

        #if defined(WINDOWS) && defined(AS_SERVICE)
        glSS.dwCurrentState  = SERVICE_STOP_PENDING;
        glSS.dwWin32ExitCode = 0;
        glSS.dwServiceSpecificExitCode = 0;
        glSS.dwCheckPoint    = 3;
        SetServiceStatus(glSS_H,&glSS);
        #endif

        log << LPROG(0) << "Shutdown data server..." << endl;
        _server_status().data_server()->shutdown();
        _server_status().data_server()->release();
        log << LPROG(0) << "Shutdown data server...OK" << endl;

        log << LPROG(0) << "Shutdown storages..." << endl;
        _server_status().storages()->release();
        log << LPROG(0) << "Shutdown storages...OK" << endl;


        #if defined(WINDOWS) && defined(AS_SERVICE)
        glSS.dwCurrentState  = SERVICE_STOP_PENDING;
        glSS.dwWin32ExitCode = 0;
        glSS.dwServiceSpecificExitCode = 0;
        glSS.dwCheckPoint    = 4;
        SetServiceStatus(glSS_H,&glSS);
        #endif

        if (_server_status().database())
        {
            log << LPROG(0) << "Disconnect from db..." << endl;
            _server_status().database()->disconnect();
            delete _server_status().database();
            _server_status().database(NULL);
            log << LPROG(0) << "Disconnect from db...OK" << endl;

            #if defined(WINDOWS) && defined(AS_SERVICE)
            glSS.dwCurrentState  = SERVICE_STOP_PENDING;
            glSS.dwWin32ExitCode = 0;
            glSS.dwServiceSpecificExitCode = 0;
            glSS.dwCheckPoint    = 3;
            SetServiceStatus(glSS_H,&glSS);
            #endif
        }

        log << LPROG(0) << "Server stopped..." << endl;

        #if defined(WINDOWS) && defined(AS_SERVICE)
        glSS.dwCurrentState  = SERVICE_STOPPED;
        glSS.dwWin32ExitCode = 0;
        glSS.dwServiceSpecificExitCode = 0;
        glSS.dwCheckPoint    = 0;
        SetServiceStatus(glSS_H,&glSS);
        #endif
    }
    catch(_error &e)
    {
        log << LPROG(0) << "Error while shutdown: " << e.str() << endl;

        #if defined(WINDOWS) && defined(AS_SERVICE)
        glSS.dwCurrentState  = SERVICE_STOPPED;
        glSS.dwWin32ExitCode = e.num();
        glSS.dwServiceSpecificExitCode = e.num();
        glSS.dwCheckPoint    = 0;
        SetServiceStatus(glSS_H,&glSS);
        #endif
    }

    return 0;
}

// ----------------------------------------------
void _config_file::parse_done() const
{
    bool ver_checked = false;

	_config_parser::_params_map::const_iterator it=cm_params_map.begin();
    while(it!=cm_params_map.end())
    {
        if      (it->first=="main_server") _server_status().main_server_name(it->second);
        else log << LPROG(0) << "Warning: unknown param '" << it->first << "'" << endl;

		it++;
    }

    if(_server_status().main_server_name().empty()) THROW_EXCEPTION("Not define main server name & main server conrtol port in local configuration file");
}

/*
// ----------------------------------------------------------
void test_storages()
{
    log << LPROG(0) << "Test storages..." << endl;

    int n=0;
    while(n<300)
    {
        string fname;
        _storages::_index ai1;
        _storages::_index ai2;
        _storages::_index ai3;
        _storages::_index ai4;
        _storages::_index ai5;
        _storages::_index ai6;

        ai1=_server_status().storages()->alloc_file(1,"video",100*1024*1024,fname,"avi");
        ofstream f1;
        f1.open(fname.c_str(),ios::out);
        f1.seekp(25*1024*1024,ios::beg);
        f1.write("1",1);
        f1.close();

        ai2=_server_status().storages()->alloc_file(1,"video",100*1024*1024,fname,"avi");
        f1.open(fname.c_str(),ios::out);
        f1.seekp(12*1024*1024,ios::beg);
        f1.write("1",1);
        f1.close();

        ai3=_server_status().storages()->alloc_file(1,"audio",10*1024*1024,fname,"mp3");
        f1.open(fname.c_str(),ios::out);
        f1.seekp(5*1024*1024,ios::beg);
        f1.write("1",1);
        f1.close();

        ai4=_server_status().storages()->alloc_file(1,"audio",10*1024*1024,fname,"mp3");
        f1.open(fname.c_str(),ios::out);
        f1.seekp(2*1024*1024,ios::beg);
        f1.write("1",1);
        f1.close();

        ai5=_server_status().storages()->alloc_file(1,"png",10*1024*1024,fname,"png");
        f1.open(fname.c_str(),ios::out);
        f1.seekp(1*1024*1024,ios::beg);
        f1.write("1",1);
        f1.close();

        ai6=_server_status().storages()->alloc_file(1,"png",10*1024*1024,fname,"png");
        f1.open(fname.c_str(),ios::out);
        f1.seekp(1*1024*1024,ios::beg);
        f1.write("1",1);
        f1.close();

        _server_status().storages()->commit_file(ai1,25*1024*1024);
        _server_status().storages()->commit_file(ai2,12*1024*1024);
        _server_status().storages()->commit_file(ai3,5*1024*1024);
        _server_status().storages()->commit_file(ai4,2*1024*1024);
        _server_status().storages()->commit_file(ai5,1*1024*1024);
        _server_status().storages()->commit_file(ai6,1*1024*1024);


        n++;
    }

    log << LPROG(0) << "Test storages done" << endl;
}
*/
