#include <map>
#include <file_utils.hpp>
#include <list>
#include <clthread.hpp>
#include <text_help.hpp>
#include <error.hpp>
#include <math.h>
#include "storages.hpp"
#include <interval.hpp>
#include <time.hpp>
#include <iostream>
#include <fstream>

#define FLOW_SIZE_K   0.1
#define DEBUG        10

//int split_file_name(const char *fname,_time_interval &itv);
//int make_file_name(const _time_interval &itv,char *fname);

//static _chanel_map chanel_map;

#include <log.hpp>
#define MLOG(A) _log() << _log::_set_module_level("storage",(A))


class _storage
{
    protected:

        std::string cm_path;
        int64 cm_max_size;

        struct _f_info
        {
            int64 size;
        };
        typedef map<_time,_f_info> _fs_map;
        _fs_map fs_map;

        struct _alloc_f_info
        {
            int64 req_size;
            std::string f_name;
            //std::string path;
        };

        typedef map<int,_alloc_f_info> _alloc_map;
        _alloc_map alloc_map;

        int64 cm_current_size;

        int cm_last_index;

        static int cm_next_number;
        int cm_number;

    public:

        _storage(const std::string &path,int64 max_size,const std::string &ftypes);

        const std::string &path() const { return cm_path; };
        int number() const { return cm_number; };

        int64 max_size() const { return cm_max_size; };

        bool accept_type(const std::string &ftype);

        bool is_full() const { if (cm_current_size>=cm_max_size) return true; return false; };
        bool will_full(int64 add) const { if (cm_current_size+add*(1+FLOW_SIZE_K)>=cm_max_size) return true; return false; };

        const _time &oldest_file_time() const { return (*fs_map.begin()).first; };

        int alloc_file(const _time &t,int64 max_file_size,const std::string ftype,std::string &fname);
        void commit_file(int alloc_index,int64 file_len);
        void remove_oldest_file();

        //void get_files_at(int y,int m,int d,list<std::string> &files);
        //void get_files_at(int y,int m,int d,unsigned char **text,int &text_len,int &text_pos);
        //bool have_files_at(int y,int m,int d);

        //void load_record(_time_interval itv,unsigned char **data,int *data_len);
};

int _storage::cm_next_number = 0;

// ------------------------------------------------------------
// ------------------------------------------------------------
class _sts_imp:public _storages
{
    protected:

      typedef map<std::string,_storage *> _sts_map;
      _sts_map sts_map;

      _locker lock;

    public:

	  _sts_imp();
      virtual ~_sts_imp();

      virtual void add_storage(const std::string &path,int64 max_size,const std::string ftypes);

      virtual int alloc_file(const _time &t,int64 max_file_size,const std::string &ftype,std::string &fname);
      virtual void commit_file(int alloc_index,int64 file_len);

      //virtual void get_files_at(int y,int m,int d,list<std::string> &files);
      //virtual void get_files_at(int y,int m,int d,unsigned char **text,int &text_len,int &text_pos);
      //virtual bool have_files_at(int y,int m,int d);
      //virtual const _chanel_map &get_chanel_map() const { return chanel_map; };
      //virtual void load_record(_time_interval itv,unsigned char **data,int *data_len);
};

// ------------------------------------------------------------
_sts_imp::_sts_imp()
{
}

// ------------------------------------------------------------
_sts_imp::~_sts_imp()
{
    _sts_map::iterator it = sts_map.begin();
    while(it!=sts_map.end())
    {
        delete (*it).second;
        it++;
    }
    sts_map.clear();
}

// ------------------------------------------------------------
void _sts_imp::add_storage(const std::string &path,int64 max_size,const std::string ftypes)
{
    MLOG(DEBUG) << "Create storage (" << path << "," << max_size << "," << ftypes << ") & add to storages..." << endl;

    std::string rpath = path;
    #ifdef WINDOWS
        if (rpath[rpath.length()-1]!='\\') rpath+='\\';
    #else
        if (rpath[rpath.length()-1]!='/') rpath+='/';
    #endif

    _sts_map::iterator it = sts_map.find(rpath);
    if (it!=sts_map.end()) throw(_error(0,std::string("Can not create duplicated storage with path ")+rpath));

    _storage *s = new _storage(rpath,max_size,ftypes);

    sts_map[s->path()]=s;

    MLOG(DEBUG) << "Create storage (" << path << " number=" << s->number() << ") OK." << endl;
}

// ------------------------------------------------------------
int _sts_imp::alloc_file(const _time &t,int64 max_file_size,const std::string &ftype,std::string &fname)
{
    MLOG(15) << "Alloc file " << t.stamp() << " mfs=" << max_file_size << " t=" << ftype << endl;

    lock.lock();

    _sts_map::iterator it;

    while(1)
    {
        it = sts_map.begin();
        while(it!=sts_map.end())
        {
            if (it->second->accept_type(ftype))
            {
                if (!it->second->will_full(max_file_size)) break;
            }
            it++;
        }

        if (it==sts_map.end())
        {
            MLOG(20) << "All storages is full. Clean oldest file..." << endl;

            _time last_time;
            last_time.set_now();

            _sts_map::iterator dit = sts_map.begin();
            _sts_map::iterator oldest = sts_map.begin();
            bool oldest_find=false;
            while(dit!=sts_map.end())
            {
                if (dit->second->accept_type(ftype))
                {
                    _time t = (*dit).second->oldest_file_time();

                    if (t<last_time)
                    {
                        last_time=t;
                        oldest = dit;
                        oldest_find=true;
                    }
                }
                dit++;
            }

            if (oldest_find)
                (*oldest).second->remove_oldest_file();
            else
            {
                lock.unlock();
                throw _error(0,"Can not allocate file in storages - wrong file type (no one storage accept it)");
            }
        }
        else break;
    }

    int index = it->second->alloc_file(t,max_file_size,ftype,fname) << 16;
    index+=(*it).second->number();

    lock.unlock();

    MLOG(15) << "File " << t.stamp() << " allocated. Index=" << index << endl;

    return index;
}

// ------------------------------------------------------------
void _sts_imp::commit_file(int alloc_index,int64 file_len)
{
    MLOG(15) << "Commit file with index=" << alloc_index << endl;

    lock.lock();

    int number=alloc_index & 0xFF;

    _sts_map::iterator it = sts_map.begin();
    while(it!=sts_map.end())
    {
        if (number==it->second->number())
        {
            it->second->commit_file(alloc_index >> 16,file_len);
            MLOG(15) << "Commit file with index=" << alloc_index << " OK" << endl;

            lock.unlock();
            return;
        }
        it++;
    }

    throw _error(alloc_index,"Can not commit file with this index. No such storage.");
}

/*
// ------------------------------------------------------------
void _sts_imp::get_files_at(int y,int m,int d,list<std::string> &files)
{
    files.clear();

    lock.lock();
    _sts_map::iterator it = sts_map.begin();
    while(it!=sts_map.end())
    {
        (*it).second->get_files_at(y,m,d,files);
        it++;
    }
    lock.unlock();
}

// ------------------------------------------------------------
void _sts_imp::get_files_at(int y,int m,int d,unsigned char **text,int &text_len,int &text_pos)
{
    lock.lock();
    _sts_map::iterator it = sts_map.begin();
    while(it!=sts_map.end())
    {
        (*it).second->get_files_at(y,m,d,text,text_len,text_pos);
        it++;
    }
    lock.unlock();

    (*text)[text_pos]='\0';
    text_pos++;
}

// ------------------------------------------------------------
bool _sts_imp::have_files_at(int y,int m,int d)
{
    lock.lock();
    _sts_map::iterator it = sts_map.begin();
    while(it!=sts_map.end())
    {
        if ((*it).second->have_files_at(y,m,d))
        {
            lock.unlock();
            return true;
        }
        it++;
    }

    lock.unlock();
    return false;
}

// ------------------------------------------------------------
void _sts_imp::load_record(_time_interval itv,unsigned char **data,int *data_len)
{
    lock.lock();
    *data_len=0;
    _sts_map::iterator it = sts_map.begin();
    while(it!=sts_map.end())
    {
        (*it).second->load_record(itv,data,data_len);
        if (*data_len!=0)
        {
            lock.unlock();
            return;
        }
        it++;
    }
    lock.unlock();
}
*/

// ------------------------------------------------------------
// ------------------------------------------------------------
_storage::_storage(const std::string &path,int64 max_size,const std::string &ftypes)
:cm_path(path),cm_max_size(max_size),cm_last_index(0),cm_current_size(0)
{

    // ------------------------------------------------------------
    int err = fu_create_path(cm_path);
    if (err) throw _error(err,std::string("Can not create storage path ")+cm_path);

    int64 count=0;

    /*
    WIN32_FIND_DATA fd;
    std::string mask = path+"*.*";
    HANDLE h=FindFirstFile(mask.c_str(),&fd);
    if (h!=INVALID_HANDLE_VALUE)
    {
        int pos=mask.rfind('\\');
        if (pos==std::string::npos) pos=mask.size()-1;

        std::string dir(mask,0,pos+1);

        while(1)
        {
            if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY)
            {
                _record_info ri;
                ri.size = (int64)(fd.nFileSizeHigh * MAXDWORD) + (int64)fd.nFileSizeLow;

                _time_interval ivl;
                int res=split_file_name(fd.cFileName,ivl);

                if (res==-1)
                {
                    DeleteFile((dir+fd.cFileName).c_str());

                    /*SYSTEMTIME stm;
                    SYSTEMTIME ltm;
                    TIME_ZONE_INFORMATION zi;

                    GetTimeZoneInformation(&zi);

                    FileTimeToSystemTime(&fd.ftLastWriteTime,&stm);
                    SystemTimeToTzSpecificLocalTime(&zi,&stm,&ltm);

                    std::string new_f_name = fd.cFileName;
                    char t[128];
                    sprintf(t,"%04i%02i%02i_%02i%02i%02i",ltm.wYear,ltm.wMonth,ltm.wDay,ltm.wHour,ltm.wMinute,ltm.wSecond);
                    new_f_name.insert(16,t);
                    _log().out(std::string("I find broken file ")+dir+fd.cFileName+". Fix it. File name will be "+dir+new_f_name,0,0);
                    int res = MoveFile((dir+fd.cFileName).c_str(),(dir+new_f_name).c_str());
                    if (!res)
                    {
                        sprintf(t,"Rename error %i",GetLastError());
                        _log().out(t,0,0);
                    }

                    split_file_name(new_f_name.c_str(),ivl);
                    */




                    /*
                }
                // TO DO : delete file with wrong name (res==-2)
                else if (res==-2)
                {
                }
                else
                {
                    ri.interval = ivl;
                    records_map[ivl.begin()] = ri;

                    current_size+=ri.size;
                    count++;
                }

            }
            if (!FindNextFile(h,&fd)) break;
        }

        FindClose(h);
    }
    */

    MLOG(0) << "Storage " << path << ". Number=" << cm_number << ". Have " << count << " files. Size = " << cm_current_size << " bytes" << endl;
}

// ------------------------------------------------------------
int _storage::alloc_file(const _time &t,int64 max_file_size,const std::string ftype,std::string &fname)
{
    char fn[MAX_PATH*2];

    strcpy(fn,cm_path.c_str());
    t.stamp_to_str(fn+cm_path.length());
    strcat(fn,"._");

    int index=cm_last_index;
    int64 size = max_file_size * (1 + FLOW_SIZE_K);

    _alloc_f_info afi;
    afi.req_size = size;
    afi.f_name = fn;
    //afi.path=_path;

    alloc_map[cm_last_index] = afi;
    cm_current_size+=size;

    cm_last_index++;
    if (cm_last_index>0xff) cm_last_index=0;

    return index;
}

// ------------------------------------------------------------
void _storage::commit_file(int alloc_index,int64 file_len)
{
    _alloc_map::iterator it = alloc_map.find(alloc_index);
    if (it==alloc_map.end())
    {
        MLOG(0) << "_storage::commit_file - WARNING!!! Can not find index " << alloc_index << " in alloc_map" << endl;
        return;
    }

    cm_current_size += file_len;
    cm_current_size -= it->second.req_size;

    char fn[MAX_PATH*2];
    strcpy(fn,it->second.f_name);
    strcat(fn+it->second.f_name.length()-2,it->second.f_type.c_str());

    MLOG(20) << "Commit file " <<  << " done. Rename it to " << _path << new_f_name << endl;
#endif

    int res = MoveFile((_path+(*it).second.f_name).c_str(),(_path+new_f_name).c_str());
    if (!res)
    {
#ifdef STORAGE_USE_LOG
        MLOG(0) << "_sts_imp::_storage::commit_file - ERROR!!! Can not rename " << _path << (*it).second.f_name << " to " << _path << new_f_name << endl;
#endif
    }

    _record_info ri;

    split_file_name(new_f_name.c_str(),ri.interval);

    ri.size=file_len;
    records_map[ri.interval.begin()] = ri;

    alloc_map.erase(it);
}

// ------------------------------------------------------------
void _sts_imp::_storage::remove_oldest_file()
{
    _records_map::iterator it = records_map.begin();
    if (it==records_map.end()) return;

    char tmp[MAX_PATH];
    make_file_name((*it).second.interval,tmp);
    std::string fname=_path+tmp;

#ifdef STORAGE_USE_LOG
    MLOG(STS_DEBUG_LEVEL) << "Remove oldest file " << fname << endl;
#endif

    int res = DeleteFile(fname.c_str());
    if (res)
    {
        current_size-=(*it).second.size;
        records_map.erase(it);
    }
    else
    {
#ifdef STORAGE_USE_LOG
        MLOG(0) << "_sts_imp::_storage::remove_oldest_file - ERROR!!! Can not remove oldest file " << fname << endl;
#endif
    }
}

/*
// ------------------------------------------------------------
void _sts_imp::_storage::get_files_at(int y,int m,int d,list<std::string> &files)
{
    int t=GetTickCount();

    _time b(y,m,d,0,0,0,0);
    b.tick(-1000*60*60);  // - one hour
    _time e(y,m,d,0,0,0,0);
    e.tick(1000*60*60*24);  // + one day

    _records_map::iterator it=records_map.upper_bound(b);

    int t2=GetTickCount();

    b.tick(1000*60*60);  // + one hour

    while(it!=records_map.end())
    {
        if ((*it).first>e) break;
        if ((*it).second.interval.end()<b)
        {
            it++;
            continue;
        }

        char time_ch_b[128];

        // more fast, but unsafe
        (*it).second.interval.begin().stamp_to_str(time_ch_b);
        time_ch_b[18]=',';
        (*it).second.interval.end().stamp_to_str(time_ch_b+19);

        files.push_back(time_ch_b);

        it++;
    }
}

// ------------------------------------------------------------
void _sts_imp::_storage::get_files_at(int y,int m,int d,unsigned char **text,int &text_len,int &text_pos)
{
    _time b(y,m,d,0,0,0,0);
    b.tick(-1000*60*60);  // - one hour
    _time e(y,m,d,0,0,0,0);
    e.tick(1000*60*60*24);  // + one day

    _records_map::iterator it=records_map.upper_bound(b);

    b.tick(1000*60*60);  // + one hour

    while(it!=records_map.end())
    {
        if ((*it).first>e) break;
        if ((*it).second.interval.end()<b)
        {
            it++;
            continue;
        }

        if (text_pos+50>text_len)
        {
            unsigned char *nm=new unsigned char [text_len*2];
            memcpy(nm,*text,text_len);
            nm[text_len]='\0';
            text_len*=2;
            delete *text;
            *text=nm;
        }

        (*it).second.interval.begin().stamp_to_str((char *)&(*text)[text_pos]);
        (*text)[text_pos+18]=',';
        (*it).second.interval.end().stamp_to_str((char *)&(*text)[text_pos+19]);
        (*text)[text_pos+37]='\r';
        (*text)[text_pos+38]='\n';
        text_pos+=39;

        it++;
    }
}

// ------------------------------------------------------------
bool _sts_imp::_storage::have_files_at(int y,int m,int d)
{
    _time b(y,m,d,0,0,0,0);
    b.tick(-1000*60*60);  // - one hour
    _time e(y,m,d,0,0,0,0);
    e.tick(1000*60*60*24);  // + one day

    _records_map::iterator it=records_map.upper_bound(b);

    b.tick(1000*60*60);  // + one hour

    while(it!=records_map.end())
    {
        if ((*it).first>e) break;
        if ((*it).second.interval.end()<b)
        {
            it++;
            continue;
        }

        return true;
    }

    return false;
}

// ------------------------------------------------------------
void _sts_imp::_storage::load_record(_time_interval itv,unsigned char **data,int *data_len)
{
    *data_len=0;

    _records_map::iterator it=records_map.find(itv.begin());
    if (it==records_map.end()) return;

    char fname[MAX_PATH];
    make_file_name(itv,fname);
    std::string file_name=_path+fname;

    ifstream f;
    f.open(file_name.c_str(),ios::in | ios::binary);

    *data_len = (*it).second.size;

    *data = new unsigned char [*data_len];
    f.read((char *)*data,*data_len);

    f.close();
}
*/

// ------------------------------------------------------------
// ------------------------------------------------------------
_storages *create_storages()
{
    return new _sts_imp();
}

/*
// ------------------------------------------------------------
int split_file_name(const char *fname,_time_interval &itv)
{
    itv.end(_time());

    int len=strlen(fname);

    if (len<17) return -2;
    if (fname[16]=='_') return -1;
    if (len<16+16+4+1) return -2;


    int y=(fname[0]-'0')*1000+(fname[1]-'0')*100+(fname[2]-'0')*10+(fname[3]-'0');
    int m=(fname[4]-'0')*10+(fname[5]-'0');
    int d=(fname[6]-'0')*10+(fname[7]-'0');
    int h=(fname[9]-'0')*10+(fname[10]-'0');
    int n=(fname[11]-'0')*10+(fname[12]-'0');
    int s=(fname[13]-'0')*10+(fname[14]-'0');

    char ch_name[128];
    strncpy(ch_name,&fname[16+16],len-16-16-4);
    ch_name[len-16-16-4]='\0';
    int ch_id=chanel_map.get_id(ch_name);

    itv.begin(_time(y,m,d,h,n,s,ch_id));

    y=(fname[0+16]-'0')*1000+(fname[1+16]-'0')*100+(fname[2+16]-'0')*10+(fname[3+16]-'0');
    m=(fname[4+16]-'0')*10+(fname[5+16]-'0');
    d=(fname[6+16]-'0')*10+(fname[7+16]-'0');
    h=(fname[9+16]-'0')*10+(fname[10+16]-'0');
    n=(fname[11+16]-'0')*10+(fname[12+16]-'0');
    s=(fname[13+16]-'0')*10+(fname[14+16]-'0');

    itv.end(_time(y,m,d,h,n,s,0));

    return 0;
}

// ------------------------------------------------------------
int make_file_name(const _time_interval &itv,char *fname)
{
    _time::_ys b = itv.begin().split();
    _time::_ys e = itv.end().split();

    sprintf(fname,"%04i%02i%02i_%02i%02i%02i_%04i%02i%02i_%02i%02i%02i_%s.264",b.y,b.m,b.d,b.h,b.n,b.s,
                                                                               e.y,e.m,e.d,e.h,e.n,e.s,
                                                                               chanel_map.chanel_for_id(b.ms).c_str());

    return 0;
}
*/

