#include "vcall_station.hpp"
#include <clthread.hpp>
#include <map>
#include <list>
#include <string.h>
#include <log.hpp>
#include <cstdlib>
#include <udefs.hpp>
#include "../common/settings.hpp"
#include "../common/stream_datagram.hpp"
#include "gl_server.hpp"
#include "net_data_cserver.hpp"

#define VBITRATE        512*1024
#define ABITRATE        9600
#define JPEG_MAX_SIZE   10*1024*1024


#define MLOG(A) _log() << _log::_set_module_level("vcall_station",(A))


class _vcs:public _vcall_station
{
    private:

        static unsigned int call_id_ref;

        _locker lock;

        typedef map<unsigned int,_call> _calls;
        _calls calls;

        // one q
        struct _queue
        {
            typedef list<unsigned int> _call_ids;  // list with calls id
            _call_ids call_ids;

            struct _work_time
            {
                _time start_at;
                _time end_at;

                _work_time() { start_at.hour(0); start_at.minute(0); end_at.hour(23); end_at.minute(59); }
            };
            _work_time work_times[7];
        };

        // map of qs
        typedef map<string,_queue> _queues;
        _queues queues;

        typedef list<_ws_info> _ws_list;
        _ws_list ws_list;

        //string tmp_dir;

        bool add_to_q(const string &q,unsigned int id);
        bool remove_from_q(const string &q,unsigned int id);
        unsigned int oldest_in_q(const string &q);

        void put_call_to_base(const _call &call);

        void close_stream_file(_call &call,_file_rec &rec);

    public:

        _vcs();
        virtual ~_vcs();

        virtual bool reg_ws(const string &ws_dns_name,_net_server_connection *c);
        virtual bool unreg_ws(_net_server_connection *c);

        virtual const _call *income_call(const string &term_dns_name,const string &qname,_net_server_connection *c,_vc_err::_code &rez);
        virtual const _call *answer_for(const string &ws_dns_name,const string &qname,const string &oper_login,_net_server_connection *c);

        virtual bool end_call(unsigned int call_id,_net_server_connection *c);
        virtual bool drop_call(_net_server_connection *c);

        virtual void queue_get_size(const string &qname,const string &ws_dns_name,int *for_ws,int *all);

        virtual unsigned int bind_data_stream_to_call(unsigned int call_id,unsigned int data_con_id,_net_server_connection *c);
        virtual unsigned int unbind_data_stream_to_call(unsigned int call_id,_net_server_connection *c);

        virtual void proc_in_data(std::string &in,unsigned int in_con_id);
        virtual void proccess_stream_datagram_for_call(unsigned int call_id,unsigned int in_con_id,const _datagram *sd);
};

unsigned int _vcs::call_id_ref = 0;

//----------------------------------------------------
_vcs::_vcs()
{
    _settings::_system::_q_servers::const_iterator qs = _settings::_global().get()->get_local_q_server();

    _transaction *tr=NULL;
    try
    {
        tr=_server_status().database()->transaction();
        tr->start();

        _db_cl::_qservers_list::_dns_name_eq c(qs->second.dns_name);
        _db_cl::_qservers_list::iterator *qs_it=_db_cl::_qservers_list().begin(tr,&c);

        _db_cl::_qs_list::iterator *q_it=(*qs_it)->queues();
        delete qs_it;

        while(!q_it->eof())
        {
            _queue q;

            _db_cl::_work_times_list::iterator *w_it=(*q_it)->work_times();
            while(!w_it->eof())
            {
                if ((*w_it)->on_day()>0 && (*w_it)->on_day()<8)
                {
                    q.work_times[(*w_it)->on_day()-1].start_at=(*w_it)->start_at();
                    q.work_times[(*w_it)->on_day()-1].end_at=(*w_it)->end_at();
                }
                (*w_it)++;
            }

            queues[(*q_it)->name()]=q;

            (*q_it)++;
        }
        delete q_it;

        tr->commit();
        delete tr;
    }
    catch(_db_error &e)
    {
        if (tr) try { tr->rollback(); delete tr; tr=NULL; } catch(...) {}
    }

    /*_settings::_q_server::_queues::const_iterator qit = qs->second.queues.begin();
    while(qit!=qs->second.queues.end())
    {
        _queue q;
        queues[qit->second.name]=q;
        qit++;
    }
    */

    MLOG(0) << "   Use " << queues.size() << " income queue(s)." << endl;
}

//----------------------------------------------------
_vcs::~_vcs()
{
}

//----------------------------------------------------
bool _vcs::reg_ws(const string &ws_dns_name,_net_server_connection *c)
{
    lock.lock();

    _ws_list::iterator wit=ws_list.begin();
    while(wit!=ws_list.end())
    {
        if (wit->dns_name==ws_dns_name) break;
        wit++;
    }

    if (wit==ws_list.end())
    {
        _ws_info i;
        i.dns_name=ws_dns_name;
        ws_list.push_front(i);
        wit=ws_list.begin();
    }

    wit->connection=c;
    wit->reg_at.set_now();
    wit->last_call_end_at.stamp(0);
    wit->last_call_end_at.tick(ws_list.size());

    lock.unlock();

    return true;
}

//----------------------------------------------------
bool _vcs::unreg_ws(_net_server_connection *c)
{
    lock.lock();

    _ws_list::iterator wit=ws_list.begin();
    while(wit!=ws_list.end())
    {
        if (wit->connection==c) break;
        wit++;
    }

    if (wit==ws_list.end())
    {
        lock.unlock();
        return false;
    }

    ws_list.erase(wit);

    lock.unlock();
    return true;
}

//----------------------------------------------------
bool _vcs::add_to_q(const string &q,unsigned int id)
{
    _queues::iterator qit = queues.find(q);
    if (qit==queues.end()) return false;

    qit->second.call_ids.push_back(id);

    return true;
}


//----------------------------------------------------
bool _vcs::remove_from_q(const string &q,unsigned int id)
{
    _queues::iterator qit = queues.find(q);
    if (qit==queues.end()) return false;

    _queue::_call_ids::iterator it = qit->second.call_ids.begin();
    while(it!= qit->second.call_ids.end())
    {
        if ((*it)==id)
        {
            qit->second.call_ids.erase(it);
            return true;
        }
        it++;
    }

    return false;
}

//----------------------------------------------------
unsigned int _vcs::oldest_in_q(const string &q)
{
    _queues::iterator qit = queues.find(q);
    if (qit==queues.end()) return 0;

    if (qit->second.call_ids.empty()) return 0;

    _queue::_call_ids::iterator it = qit->second.call_ids.begin();
    return (*it);
}

//----------------------------------------------------
const _call *_vcs::income_call(const string &term_dns_name,const string &qname,_net_server_connection *c,_vc_err::_code &rez)
{

    _queues::iterator qit=queues.find(qname);
    if (qit==queues.end())
    {
        rez=_vc_err::common_no_given_q;
        return NULL;
    }

    _time tn;
    tn.set_now();
    int dn=tn.day_of_week();
    if (dn==0) dn=7;  // sunday
    dn--;             // array index

    int start_s=qit->second.work_times[dn].start_at.hour()*60*60+qit->second.work_times[dn].start_at.minute()*60;
    int end_s=qit->second.work_times[dn].end_at.hour()*60*60+qit->second.work_times[dn].end_at.minute()*60;
    int now_s=tn.hour()*60*60+tn.minute()*60+tn.second();

    if (now_s<start_s || now_s>end_s)
    {
        rez=_vc_err::q_on_rest;
        return NULL;
    }

    unsigned int id;

    lock.lock();

    call_id_ref++;
    id=call_id_ref;

    _call call;
    call.id=id;
    call.ring_time.set_now();
    call.from_term.dns_name=term_dns_name;
    call.from_term.con=c;
    call.from_term.data_con_id=0;
    call.from_term.video_file.f_ptr=NULL;
    call.from_term.audio_file.f_ptr=NULL;

    call.answer_ws.dns_name.clear();
    call.answer_ws.con=NULL;
    call.answer_ws.data_con_id=0;
    call.answer_ws.video_file.f_ptr=NULL;
    call.answer_ws.audio_file.f_ptr=NULL;


    call.answer_time.stamp(0);
    call.end_time.stamp(0);

    call.operator_login.clear();

    call.printed_doc_cnt=0;
    call.scaned_doc_cnt=0;

    call.scan_file.f_ptr=NULL;
    call.print_file.f_ptr=NULL;

    call.in_q=qname;
    if (!add_to_q(qname,id))
    {
        rez=_vc_err::common_wrong_state;
        lock.unlock();
        return NULL;
    }

    call.end_code=END_CALL_CODE_NORMAL;

    calls[id]=call;

    _calls::iterator cit = calls.find(id);

    lock.unlock();

    rez=_vc_err::ok;

    return (&cit->second);
}

//----------------------------------------------------
const _call *_vcs::answer_for(const string &ws_dns_name,const string &qname,const string &oper_login,_net_server_connection *c)
{
    unsigned int id;

    lock.lock();

    id = oldest_in_q(qname);
    if (id==0)
    {
        lock.unlock();
        return NULL;
    }

    remove_from_q(qname,id);

    // update ws info
    _ws_list::iterator winfo_it=ws_list.begin();
    while(winfo_it!=ws_list.end())
    {
        if (winfo_it->dns_name==ws_dns_name)
        {
            winfo_it->last_call_end_at.set_now();
            // we up (by 1 day) last_call_end_at for answering opers - to prevent show calls for them then we have wait (no answering) opers
            winfo_it->last_call_end_at.tick(1000*60*60*24);
            break;
        }
        winfo_it++;
    }

    _calls::iterator cit = calls.find(id);
    if (cit==calls.end())
    {
        MLOG(0) << "WARNING: answer_call - in q was call, in calls was NOT call" << endl;
        lock.unlock();
        return NULL;
    }

    cit->second.in_q=cit->second.in_q;
    cit->second.operator_login=oper_login;
    cit->second.answer_time.set_now();
    cit->second.answer_ws.dns_name=ws_dns_name;
    cit->second.answer_ws.con=c;

    cit->second.answer_time.set_now();
    cit->second.operator_login=oper_login;

    _settings::_system::_terminals::const_iterator tit = _settings::_global().get()->terminals.begin();

    cit->second.video_rotate_angle = 0;
    while(tit != _settings::_global().get()->terminals.end())
    {
        if (tit->second.dns_name==cit->second.from_term.dns_name)
        {
            cit->second.video_rotate_angle=atoi(tit->second.video_rotate_angle.c_str());
            break;
        }
        tit++;
    }

    // open tmp files

    // terminal
    // ------------------------------------------------------------------------
    cit->second.from_term.video_file.type=_db_cl::_call_data::_dt_cv;
    cit->second.from_term.video_file.index=
                    _server_status().storages()->alloc_file(0,"video",(int64)30*60*VBITRATE/8,cit->second.from_term.video_file.ffname,"mfv");

    MLOG(0) << "Open video file for caller - " << cit->second.from_term.video_file.ffname << endl;
    cit->second.from_term.video_file.f_ptr = new ofstream;
    cit->second.from_term.video_file.f_ptr->open(cit->second.from_term.video_file.ffname.c_str(),ios::out | ios::binary);
    if (!(*cit->second.from_term.video_file.f_ptr))
    {
        MLOG(0) << "ERROR!!! Open video file - " << cit->second.from_term.video_file.ffname << endl;
        delete cit->second.from_term.video_file.f_ptr;
        cit->second.from_term.video_file.f_ptr=NULL;
        _server_status().storages()->commit_file(cit->second.from_term.video_file.index,0);
    }

    // ------------------------------------------------------------------------
    cit->second.from_term.audio_file.type=_db_cl::_call_data::_dt_ca;
    cit->second.from_term.audio_file.index=
                    _server_status().storages()->alloc_file(0,"audio",(int64)30*60*ABITRATE/8,cit->second.from_term.audio_file.ffname,"mfa");

    MLOG(0) << "Open audio file for caller - " << cit->second.from_term.audio_file.ffname << endl;
    cit->second.from_term.audio_file.f_ptr = new ofstream;
    cit->second.from_term.audio_file.f_ptr->open(cit->second.from_term.audio_file.ffname,ios::out | ios::binary);
    if (!(*cit->second.from_term.audio_file.f_ptr))
    {
        MLOG(0) << "ERROR!!! Open audio file - " << cit->second.from_term.audio_file.ffname << endl;
        delete cit->second.from_term.audio_file.f_ptr;
        cit->second.from_term.audio_file.f_ptr=NULL;
        _server_status().storages()->commit_file(cit->second.from_term.audio_file.index,0);
    }

    // answer
    // ------------------------------------------------------------------------
    cit->second.answer_ws.video_file.type=_db_cl::_call_data::_dt_av;
    cit->second.answer_ws.video_file.index=
                    _server_status().storages()->alloc_file(0,"video",(int64)30*60*VBITRATE/8,cit->second.answer_ws.video_file.ffname,"mfv");

    MLOG(0) << "Open video file for answer - " << cit->second.answer_ws.video_file.ffname << endl;
    cit->second.answer_ws.video_file.f_ptr = new ofstream;
    cit->second.answer_ws.video_file.f_ptr->open(cit->second.answer_ws.video_file.ffname.c_str(),ios::out | ios::binary);
    if (!(*cit->second.answer_ws.video_file.f_ptr))
    {
        MLOG(0) << "ERROR!!! Open video file - " << cit->second.answer_ws.video_file.ffname << endl;
        delete cit->second.answer_ws.video_file.f_ptr;
        cit->second.answer_ws.video_file.f_ptr=NULL;
        _server_status().storages()->commit_file(cit->second.answer_ws.video_file.index,0);
    }

    // ------------------------------------------------------------------------
    cit->second.answer_ws.audio_file.type=_db_cl::_call_data::_dt_aa;
    cit->second.answer_ws.audio_file.index=
                    _server_status().storages()->alloc_file(0,"audio",(int64)30*60*ABITRATE/8,cit->second.answer_ws.audio_file.ffname,"mfa");

    MLOG(0) << "Open audio file for answer - " << cit->second.answer_ws.audio_file.ffname << endl;
    cit->second.answer_ws.audio_file.f_ptr = new ofstream;
    cit->second.answer_ws.audio_file.f_ptr->open(cit->second.answer_ws.audio_file.ffname,ios::out | ios::binary);
    if (!(*cit->second.answer_ws.audio_file.f_ptr))
    {
        MLOG(0) << "ERROR!!! Open audio file - " << cit->second.answer_ws.audio_file.ffname << endl;
        delete cit->second.answer_ws.audio_file.f_ptr;
        cit->second.answer_ws.audio_file.f_ptr=NULL;
        _server_status().storages()->commit_file(cit->second.answer_ws.audio_file.index,0);
    }

    lock.unlock();

    return (&cit->second);
}

//----------------------------------------------------
void _vcs::close_stream_file(_call &call,_file_rec &r)
{
    if (r.f_ptr)
    {
        int64 len=r.f_ptr->tellp();
        _server_status().storages()->commit_file(r.index,len);

        r.f_ptr->close();
        delete r.f_ptr;
        r.f_ptr=NULL;

        call.files.push_back(r);
    }
}

//----------------------------------------------------
bool _vcs::end_call(unsigned int call_id,_net_server_connection *c)
{
    lock.lock();

    _calls::iterator cit = calls.find(call_id);
    if (cit==calls.end())
    {
        lock.unlock();
        return false;
    }

    cit->second.end_time.set_now();

    if (cit->second.answer_time.stamp())
    {
        cit->second.end_code=END_CALL_CODE_NORMAL;
    }
    else
    {
        cit->second.end_code=END_CALL_CODE_USER_ABORTED;
    }

    close_stream_file(cit->second,cit->second.scan_file);
    close_stream_file(cit->second,cit->second.print_file);

    close_stream_file(cit->second,cit->second.from_term.video_file);
    close_stream_file(cit->second,cit->second.from_term.audio_file);

    close_stream_file(cit->second,cit->second.answer_ws.video_file);
    close_stream_file(cit->second,cit->second.answer_ws.audio_file);

    put_call_to_base(cit->second);

    if (!cit->second.in_q.empty()) remove_from_q(cit->second.in_q,cit->second.id);
    calls.erase(cit);

    lock.unlock();
    return true;
}

//----------------------------------------------------
bool _vcs::drop_call(_net_server_connection *c)
{
    lock.lock();

    _calls::iterator cit = calls.begin();
    while(cit!=calls.end())
    {
        if (cit->second.from_term.con==c || cit->second.answer_ws.con==c)
        {
            cit->second.end_time.set_now();
            cit->second.end_code=END_CALL_CODE_CRUSHED;

            close_stream_file(cit->second,cit->second.scan_file);
            close_stream_file(cit->second,cit->second.print_file);

            close_stream_file(cit->second,cit->second.from_term.video_file);
            close_stream_file(cit->second,cit->second.from_term.audio_file);

            close_stream_file(cit->second,cit->second.answer_ws.video_file);
            close_stream_file(cit->second,cit->second.answer_ws.audio_file);

            put_call_to_base(cit->second);

            if (cit->second.in_q!="") remove_from_q(cit->second.in_q,cit->second.id);
            calls.erase(cit);
            lock.unlock();
            return true;
        }
        cit++;
    }

    lock.unlock();
    return false;
}

//----------------------------------------------------
void _vcs::queue_get_size(const string &qname,const string &ws_dns_name,int *for_ws,int *all)
{
    MLOG(20) << "_vcs::queue_get_size qname=" << qname << " ws_dns_name=" << ws_dns_name << endl;

    *all = 0;
    *for_ws=0;

    lock.lock();

    // find q
    _queues::iterator qit = queues.find(qname);
    if (qit==queues.end())
    {
        lock.unlock();
        return;
    }

    *all = qit->second.call_ids.size();
    if (*all==0)
    {
        lock.unlock();
        return;
    }

    // find ws info
    _ws_list::iterator wit=ws_list.begin();
    while(wit!=ws_list.end())
    {
        if (wit->dns_name==ws_dns_name) break;
        wit++;
    }
    if (wit==ws_list.end())
    {
        MLOG(0) << "Warning: get q size for ws with dns name = " << ws_dns_name << " but it is not in reg ws list." << endl;
        lock.unlock();
        return;
    }

    // WS_N
    // ....
    // WS_2
    // WS_1
    //      C1 C2 C3

    // we "sort" WS by last call end time - position is y
    // we "sort" calls by income time - position x

    // calc ws y pos
    int y=0;
    _ws_list::iterator cwit=ws_list.begin();
    while(cwit!=ws_list.end())
    {
        if (cwit->last_call_end_at<wit->last_call_end_at) y++;
        cwit++;
    }

    _time now;
    now.set_now();

    int ws_list_size=ws_list.size();

    MLOG(20) << "_vcs::queue_get_size (" << ws_dns_name << ") y=" << y << " ws_list_size=" << ws_list_size << endl;

    int x=0;
    int n=0;
    _queue::_call_ids::iterator cid_it=qit->second.call_ids.begin();
    while (cid_it!=qit->second.call_ids.end())
    {
        _calls::iterator cit=calls.find(*cid_it);

        _time::time_val_i time_diff=now.stamp()-cit->second.ring_time.stamp();
        time_diff=time_diff/10000000;
        time_diff=time_diff/15;

        int start_y=x;
        int end_y=x+time_diff;

        MLOG(20) << "_vcs::queue_get_size (" << ws_dns_name << ") y=" << y << " n=" << n << " x=" << x << " start_y=" << start_y << " end_y=" << end_y << endl;

        if (end_y<ws_list_size)
        {
            if (start_y<=y && y<=end_y) (*for_ws)++;
        }
        else
        {
            end_y-=ws_list_size;
            if (end_y<start_y)
            {
                if (start_y<=y) (*for_ws)++;
                if (y<=end_y) (*for_ws++);
            }
            else (*for_ws)++;
        }

        cid_it++;
        x++;
        if (x==ws_list_size) x=0;
        n++;
    }

    MLOG(20) << "_vcs::queue_get_size (" << ws_dns_name << ") for_ws=" << (*for_ws) << endl;

    lock.unlock();
}

//----------------------------------------------------
unsigned int _vcs::bind_data_stream_to_call(unsigned int call_id,unsigned int data_con_id,_net_server_connection *cc)
{
    // at first function call bind we JUST set data stream id on terminal or answer_ws side
    // at second function call second_id!=0 (from another side) and we call net_data_server to bind two connections

    lock.lock();

    MLOG(10) << "Bind data stream " << data_con_id << " to call " << call_id << " ..." << endl;

    _calls::iterator cit = calls.find(call_id);
    if (cit==calls.end())
    {
        MLOG(0) << "WARNING!!! Bind data stream " << data_con_id << " to call " << call_id << " ...no such call" << endl;
        lock.unlock();
        return 0;
    }

    unsigned int second_id = 0;

    if      (cit->second.from_term.con==cc)
    {
        cit->second.from_term.data_con_id=data_con_id;
        second_id = cit->second.answer_ws.data_con_id;
        MLOG(10) << "Bind data stream " << data_con_id << " to call " << call_id << " ...its caller side" << endl;
    }
    else if (cit->second.answer_ws.con==cc)
    {
        cit->second.answer_ws.data_con_id=data_con_id;
        second_id = cit->second.from_term.data_con_id;
        MLOG(10) << "Bind data stream " << data_con_id << " to call " << call_id << " ...its answer side" << endl;
    }
    else
    {
        MLOG(0) << "WARNING!!! Bind data stream " << data_con_id << " to call " << call_id << " ...no point of connection with given data_con_id" << endl;
        lock.unlock();
        return 0;
    }

    lock.unlock();

    if (second_id)
    {
        MLOG(10) << "_vcs::bind_data_stream_to_call - data streams " << data_con_id << " & " << second_id << " was binded to call " << call_id << ". Send bind datagram to one client..." << endl;

        _datagram sd((unsigned char *)&second_id,sizeof(unsigned int),_stream_datagram::second_side_bind);

        // WE send bind only to first connection
        // Client on first side must send to second side bind datagram itself
        _net_server_connection *dc=_server_status().data_server()->get_con(data_con_id);
        if (dc)
        {
            while(!dc->locker()->try_lock()) Sleep(1);
            dc->to_out(std::string((char *)sd.data(),sd.size()));
            dc->locker()->unlock();
            _server_status().data_server()->free_con(data_con_id);
        }
        else
        {
            MLOG(0) << "WARNING!!! In _vcs::bind_data_stream_to_call - data streams " << data_con_id << " & " << second_id
                    << " was binded to call " << call_id << ". Send bind datagram to one client failed...no data connection by id=" << data_con_id << endl;
            // MAY BE DROP CALL ?
            return second_id;
        }
        MLOG(10) << "_vcs::bind_data_stream_to_call - data streams " << data_con_id << " & " << second_id << " was binded to call " << call_id << ". Send bind datagram to one client...done" << endl;

        return second_id;
    }
    else
    {
        MLOG(10) << "_vcs::bind_data_stream_to_call - data stream " << data_con_id << " was binded to call " << call_id << ". Wait second side..." << endl;

        return data_con_id;
    }
}

//----------------------------------------------------
unsigned int _vcs::unbind_data_stream_to_call(unsigned int call_id,_net_server_connection *cc)
{
    lock.lock();

    MLOG(10) << "UNBIND data stream from call " << call_id << endl;

    _calls::iterator cit = calls.find(call_id);
    if (cit==calls.end())
    {
        MLOG(10) << "WARNING!!! UNBind data stream to call...no such call" << endl;
        lock.unlock();
        return 0;
    }

    unsigned int second_id = 0;
    unsigned int unbind_id = 0;

    if      (cit->second.from_term.con==cc)
    {
        MLOG(10) << "UNBIND data stream " << cit->second.from_term.data_con_id << " from call " << call_id << "...its caller side." << endl;
        unbind_id=cit->second.from_term.data_con_id;
        cit->second.from_term.data_con_id=0;
        second_id = cit->second.answer_ws.data_con_id;
    }
    else if (cit->second.answer_ws.con==cc)
    {
        MLOG(10) << "UNBIND data stream " << cit->second.answer_ws.data_con_id << " from call " << call_id << "...its answer side." << endl;
        unbind_id=cit->second.answer_ws.data_con_id;
        cit->second.answer_ws.data_con_id=0;
        second_id = cit->second.from_term.data_con_id;
    }
    else
    {
        MLOG(10) << "WARNING!!! UNBind data stream from call " << call_id << "...no point of connection" << endl;
        lock.unlock();
        return 0;
    }

    lock.unlock();

    // data stream was binded ?
    if (second_id)
    {
        MLOG(10) << "UNBIND data stream " << unbind_id << " from call " << call_id << " done. All data connections was unbinded from this call." << endl;
        return 2;
    }
    else
    {
        MLOG(10) << "UNBIND data stream " << unbind_id << " from call " << call_id << " done. One data connections was unbinded from this call." << endl;
        return 1;
    }
}

//----------------------------------------------------
void _vcs::put_call_to_base(const _call &call)
{
    lock.lock();

    char tmp[256];
    sprintf(tmp,"Write data about call to db (call_id=%u)...",call.id);
    MLOG(0) << tmp << endl;

    try
    {
        _transaction* tr = _server_status().database()->transaction();

        _db_cl::_call* cdb = new _db_cl::_call(tr);

        // server & q
        string in_q = call.in_q;
        _db_cl::_qserver *qsrv = new _db_cl::_qserver(_server_status().self_db_id(), tr);
        if(!qsrv)
        {
            cdb->donot_save();
            delete cdb;

            sprintf(tmp,"ERROR!!! Write data about call to db (call_id=%u)...no such q server in db (q server.id=%u)",call.id,_server_status().self_db_id().value());
            MLOG(0) << tmp << endl;
        }
        _db_cl::_qs_list::_name_eq q_it_cond(in_q);
        _db_cl::_qs_list::iterator *q_it = _db_cl::_qs_list().begin(tr, &q_it_cond);
        if(q_it->eof())
        {
            cdb->donot_save();
            delete cdb;
            delete q_it;
            delete qsrv;

            sprintf(tmp, "ERROR!!! Write data about call to db (call_id=%u)...no such q in db (q.name=%s & q server.id=%u)",
                    call.id, in_q.c_str(), qsrv->id().value());
            MLOG(0) << tmp << endl;
        }
        cdb->q((*q_it));
        delete q_it;
        delete qsrv;

        // terminal
        _db_cl::_terminals_list::_dns_name_eq tit_cond(call.from_term.dns_name);
        _db_cl::_terminals_list::iterator *tit = _db_cl::_terminals_list().begin(tr,&tit_cond);
        if (tit->eof())
        {
            cdb->donot_save();
            delete cdb;
            delete tit;

            sprintf(tmp,"ERROR!!! Write data about call to db (call_id=%u)...no such terminal in db (terminal.dns_name=%s)",call.id,call.from_term.dns_name.c_str());
            MLOG(0) << tmp << endl;
        }
        cdb->from_terminal((*tit));
        delete tit;

        // workstation
        if (!call.answer_ws.dns_name.empty())
        {
            _db_cl::_workstations_list::_dns_name_eq wit_cond(call.answer_ws.dns_name);
            _db_cl::_workstations_list::iterator *wit = _db_cl::_workstations_list().begin(tr,&wit_cond);
            if (wit->eof())
            {
                cdb->donot_save();
                delete cdb;
                delete wit;

                sprintf(tmp,"ERROR!!! Write data about call to db (call_id=%u)...no such workstation in db (workstation.dns_name=%s)",call.id,call.answer_ws.dns_name.c_str());
                MLOG(0) << tmp << endl;
            }
            cdb->answer_workstation((*wit));
            delete wit;

            // update ws info
            _ws_list::iterator winfo_it=ws_list.begin();
            while(winfo_it!=ws_list.end())
            {
                if (winfo_it->dns_name==call.answer_ws.dns_name)
                {
                    winfo_it->last_call_end_at=call.end_time;
                    break;
                }
                winfo_it++;
            }
        }
        else
        {
            cdb->answer_workstation(NULL);
        }

        // operator
        if (call.operator_login.length())
        {
            _db_cl::_users_list::_name_eq uit_cond(call.operator_login);
            _db_cl::_users_list::iterator *uit = _db_cl::_users_list().begin(tr,&uit_cond);
            if (uit->eof())
            {
                cdb->donot_save();
                delete cdb;
                delete uit;

                sprintf(tmp,"ERROR!!! Write data about call to db (call_id=%u)...no such operator in db (operator.name=%s)",call.id,call.operator_login.c_str());
                MLOG(0) << tmp << endl;
            }
            cdb->answer_operator((*uit));
            delete uit;
        }
        else
        {
            cdb->answer_operator(NULL);
        }

        cdb->begin_at(call.ring_time);
        cdb->answer_at(call.answer_time);
        cdb->end_at(call.end_time);

        cdb->end_code(call.end_code);
//TODO        cdb->printed_doc_cnt(call.printed_doc_cnt);
//TODO        cdb->scaned_doc_cnt(call.scaned_doc_cnt);
//TODO        cdb->caller_id(call.);

        list<_file_rec>::const_iterator fit=call.files.begin();
        while(fit!=call.files.end())
        {
            _db_cl::_storage *dbst = new _db_cl::_storage(fit->index.storage_id,tr);

            _db_cl::_call_data *cd = new _db_cl::_call_data(tr);
            cd->call(cdb);
            cd->storage(dbst);
            cd->doc_type(fit->type);

            // crop full name to file name only
            string fname;
            size_t pos=fit->ffname.rfind(PATH_SLASH);

            if (pos!=string::npos)
                fname=string(fit->ffname,pos+1);
            else
                fname=fit->ffname;

            cd->ref_name(fname);
            cd->comments("-");

            delete cd;
            delete dbst;
            fit++;
        }

        delete cdb;

        tr->commit();
        delete tr;
    }
    catch(_db_error &e)
    {
        sprintf(tmp,"ERROR!!! Write data about call to db (call_id=%u)...",call.id);
        MLOG(0) << tmp << endl << e.error_st() << endl;
        lock.unlock();

        return;
        //throw(_error(0,e.error_st()));
    }

    lock.unlock();
}

//----------------------------------------------------
void _vcs::proc_in_data(std::string &in,unsigned int in_con_id)
{
    unsigned int call_id=0;
    unsigned int out_con_id=0;

    lock.lock();

    // find out connection id & call id
    _calls::iterator cit = calls.begin();
    while(cit!=calls.end())
    {
        if (cit->second.from_term.data_con_id == in_con_id)
        {
            out_con_id = cit->second.answer_ws.data_con_id;
            call_id = cit->second.id;
            break;
        }
        else if (cit->second.answer_ws.data_con_id == in_con_id)
        {
            out_con_id = cit->second.from_term.data_con_id;
            call_id = cit->second.id;
            break;
        }
        cit++;
    }

    lock.unlock();

    // if we do not find call (and/or out_con_id)
    if (!call_id || !out_con_id)
    {
        if (in.size()>10*1024*1024)
        {
            MLOG(0) << "WARNING!!! Data connection " << in_con_id << " have input buffer more then 10M (out connection = "
                    << out_con_id << " call_id = " << call_id << " ) - input buffer will be dropped..." << endl;
            in.clear();
        }
        return ;
    }

    // get connections
    _net_server_connection *in_c = _server_status().data_server()->get_con(in_con_id);
    _net_server_connection *out_c = _server_status().data_server()->get_con(out_con_id);

    // it can be broken - just log it (may be drop call ?)
    if (!in_c || !out_c)
    {
        MLOG(0) << "WARNING!!! Binded data connections (" << in_con_id << " & " << out_con_id << ") are :"
                << (in_c ? " in ok" : " in broken")
                << (out_c ? " out ok" : " out broken") << "." << endl;

        if (in_c) _server_status().data_server()->free_con(in_con_id);
        if (out_c) _server_status().data_server()->free_con(out_con_id);


        if (in.size()>10*1024*1024)
        {
            MLOG(0) << "WARNING!!! Data connection " << in_con_id << " have input buffer more then 10M (out connection = "
                    << out_con_id << " call_id = " << call_id << " ) - input buffer will be dropped..." << endl;
            in.clear();
        }
        return ;
    }

    string out_data="";

    // parse in data as datagrams, proc datagrams & resend data to second side
    // TODO - optimize it
    while(1)
    {
        _datagram *sd=NULL;

        if (in.size()>=_datagram::header_len())
        {
            int s; // raw data size
            memcpy(&s,in.data()+(_datagram::header_len()-sizeof(int)),sizeof(int));

            if (in.size()>=s+_datagram::header_len())
            {
                sd=new _datagram(in.data());
                _server_status().call_station()->proccess_stream_datagram_for_call(call_id,in_con_id,sd);
                delete sd;
                out_data+=string(in,0,s+_datagram::header_len());
                in.erase(0,s+_datagram::header_len());
            }
            else
            {
                break;
            }
        }
        else
        {
            break;
        }
    }

    if (!out_data.empty())
    {
        while(!out_c->locker()->try_lock()) Sleep(1);
        out_c->to_out(out_data);
        out_c->locker()->unlock();
    }

    // free date connections for use
    _server_status().data_server()->free_con(in_con_id);
    _server_status().data_server()->free_con(out_con_id);
}

//----------------------------------------------------
void _vcs::proccess_stream_datagram_for_call(unsigned int call_id,unsigned int in_con_id,const _datagram *sd)
{
    lock.lock();

    _calls::iterator cit = calls.find(call_id);
    if (cit==calls.end())
    {
        lock.unlock();
        return;
    }

    _call_point *cp=NULL;
    if (cit->second.from_term.data_con_id == in_con_id) cp=&(cit->second.from_term);
    if (cit->second.answer_ws.data_con_id == in_con_id) cp=&(cit->second.answer_ws);

    if (!cp)
    {
        lock.unlock();
        return;
    }

    char tmp[256];

    int s=sd->raw_size();

    if (sd->type()==_stream_datagram::not_set)
    {
    }
    else if (sd->type()==_stream_datagram::header)
    {
        if (cp->video_file.f_ptr)
        {
            cp->video_file.f_ptr->write((char *)&s,sizeof(int));
            cp->video_file.f_ptr->write((const char *)sd->raw_data(),s);
        }
    }
    else if (sd->type()==_stream_datagram::audio_frame)
    {
        if (cp->audio_file.f_ptr)
        {
            cp->audio_file.f_ptr->write((char *)&s,sizeof(int));
            cp->audio_file.f_ptr->write((const char *)sd->raw_data(),s);
        }
    }
    else if (sd->type()==_stream_datagram::video_i_frame)
    {
        if (cp->video_file.f_ptr)
        {
            cp->video_file.f_ptr->write((char *)&s,sizeof(int));
            cp->video_file.f_ptr->write((const char *)sd->raw_data(),s);
        }
    }
    else if (sd->type()==_stream_datagram::video_p_frame)
    {
        if (cp->video_file.f_ptr)
        {
            cp->video_file.f_ptr->write((char *)&s,sizeof(int));
            cp->video_file.f_ptr->write((const char *)sd->raw_data(),s);
        }
    }
    else if (sd->type()==_stream_datagram::video_bbp_frame)
    {
        if (cp->video_file.f_ptr)
        {
            cp->video_file.f_ptr->write((char *)&s,sizeof(int));
            cp->video_file.f_ptr->write((const char *)sd->raw_data(),s);
        }
    }
    else if (sd->type()==_stream_datagram::motion_event)
    {
    }
    else if (sd->type()==_stream_datagram::connection_id)
    {
    }
    else if (sd->type()==_stream_datagram::bin)
    {
    }
    else if (sd->type()==_stream_datagram::second_side_bind)
    {
    }
    else if (sd->type()==_stream_datagram::end_notify)
    {
    }
    else if (sd->type()==_stream_datagram::audio_header)
    {
        if (cp->audio_file.f_ptr)
        {
            cp->audio_file.f_ptr->write((char *)&s,sizeof(int));
            cp->audio_file.f_ptr->write((const char *)sd->raw_data(),s);
        }
    }
    else if (sd->type()==_stream_datagram::image_to_print_start)
    {
        cit->second.print_file.type = _db_cl::_call_data::_dt_ai;
        cit->second.print_file.index = _server_status().storages()->alloc_file(0,"jpg",JPEG_MAX_SIZE,cit->second.print_file.ffname,"jpg");

        MLOG(0) << "Open print file - " << cit->second.print_file.ffname << endl;
        cit->second.print_file.f_ptr = new ofstream;
        cit->second.print_file.f_ptr->open(cit->second.print_file.ffname.c_str(),ios::out | ios::binary);
        if (!(*cit->second.print_file.f_ptr))
        {
            MLOG(0) << "ERROR!!! Open print file - " << cit->second.print_file.ffname << endl;
            _server_status().storages()->commit_file(cit->second.print_file.index,0);
            delete cit->second.print_file.f_ptr;
            cit->second.print_file.f_ptr=NULL;
        }
    }
    else if (sd->type()==_stream_datagram::image_to_print)
    {
        if (cit->second.print_file.f_ptr) cit->second.print_file.f_ptr->write((const char *)sd->raw_data(),s);
    }
    else if (sd->type()==_stream_datagram::image_to_print_end)
    {
        close_stream_file(cit->second,cit->second.print_file);
        cit->second.printed_doc_cnt++;
    }
    else if (sd->type()==_stream_datagram::scan_cmd)
    {
    }
    else if (sd->type()==_stream_datagram::scan_image_start)
    {
        cit->second.scan_file.type = _db_cl::_call_data::_dt_ci;
        cit->second.scan_file.index = _server_status().storages()->alloc_file(0,"jpg",JPEG_MAX_SIZE,cit->second.scan_file.ffname,"jpg");

        MLOG(0) << "Open scan file - " << cit->second.scan_file.ffname << endl;
        cit->second.scan_file.f_ptr = new ofstream;
        cit->second.scan_file.f_ptr->open(cit->second.scan_file.ffname.c_str(),ios::out | ios::binary);
        if (!(*cit->second.scan_file.f_ptr))
        {
            MLOG(0) << "ERROR!!! Open print file - " << cit->second.scan_file.ffname << endl;
            _server_status().storages()->commit_file(cit->second.scan_file.index,0);
            delete cit->second.scan_file.f_ptr;
            cit->second.scan_file.f_ptr=NULL;
        }
    }
    else if (sd->type()==_stream_datagram::scan_image)
    {
        if (cit->second.scan_file.f_ptr) cit->second.scan_file.f_ptr->write((const char *)sd->raw_data(),s);
    }
    else if (sd->type()==_stream_datagram::scan_image_end)
    {
        close_stream_file(cit->second,cit->second.scan_file);
        cit->second.scaned_doc_cnt++;
    }
    else if (sd->type()==_stream_datagram::ref_time)
    {
    }
    else
    {
    }

    lock.unlock();
}

//----------------------------------------------------
_vcall_station *create_vcall_station()
{
    return new _vcs;
}
