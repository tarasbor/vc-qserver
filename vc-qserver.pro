#-Wl,--subsystem,windows -mthreads

TEMPLATE = app
#CONFIG += console
#CONFIG += windows
CONFIG -= app_bundle
CONFIG -= qt

TARGET = qserver

INCLUDEPATH += ../../libcommon
INCLUDEPATH += ../../libcommon/system
INCLUDEPATH += ../../libcommon/log2
INCLUDEPATH += ../../libcommon/obj_db5
INCLUDEPATH += ../../libcommon/otl4
INCLUDEPATH += ../../libcommon/thread
INCLUDEPATH += ../../libcommon/htmlreport
INCLUDEPATH += ../../libcommon/xml
INCLUDEPATH += ../../libcommon/xml/lib
INCLUDEPATH += ../../libcommon/xml/bin
INCLUDEPATH += ../../libcommon/net
#INCLUDEPATH += ../../libcommon/com
#INCLUDEPATH += ./http_obj
#INCLUDEPATH += ../oci/include
INCLUDEPATH += /opt/ibm/db2/V9.7/include

#launch under linux,db2
DEFINES += VC_DB2

CONFIG(windows) {
    DEFINES *= WINDOWS
}
#DEFINES *= __GNUC__
DEFINES -= UNICODE
DEFINES *= DB_USE_COMMON_TIME

CONFIG(debug, debug|release) {
    DEFINES += AS_APP
    DEFINES -= AS_SERVICE
    DEFINES *= "KEEP_ALIVE_TIME=0"
    message("--- debug ---")
}
else {
    DEFINES -= AS_APP
    DEFINES += AS_SERVICE
    message("--- release ---")
}


SOURCES += \
    ../../libcommon/obj_db5/base.cpp \
    ../../libcommon/thread/clthread.cpp \
    ../../libcommon/log2/log_file_writer.cpp \
    ../../libcommon/log2/log.cpp \
    ../../libcommon/system/system_env.cpp \
    ../../libcommon/system/text_help.cpp \
    ../../libcommon/net/net_http_server.cpp \
    ../../libcommon/system/time.cpp \
    ../../libcommon/net/net_http.cpp \
    ../../libcommon/net/net_server_tcp.cpp \
    ../../libcommon/net/net_lib_error.cpp \
    ../../libcommon/net/net_line_tcp.cpp \
    ../../libcommon/htmlreport/lib/htmlrep.cpp \
    ../../libcommon/system/codepage.cpp \
    ../../libcommon/system/error.cpp \
    ../../libcommon/xml/lib/ut_xml_par.cpp \
    ../../libcommon/xml/lib/ut_xml_tag.cpp \
    vcall_station.cpp \
    sys_stat_holder.cpp \
    net_data_cserver.cpp \
    net_control_cserver.cpp \
    main.cpp \
    gl_server.cpp \
    #../common/stream_datagram.cpp \
    ../common/settings.cpp \
    ../common/net_packet.cpp \
    ../common/net_input.cpp \
    ../common/net_control_client.cpp \
    ../common/console.cpp \
    ../common/chanel_map.cpp \
    ../common/db_obj/workstation.cpp \
    ../common/db_obj/user.cpp \
    ../common/db_obj/terminal.cpp \
    ../common/db_obj/server.cpp \
    ../common/db_obj/constant.cpp \
    ../common/db_obj/call.cpp \
    ../common/db_obj/queue.cpp \
    ../../libcommon/system/config_parser.cpp \
    ../common/db_obj/storage.cpp \
    ../../libcommon/obj_db5/base_db2.cpp \
    ../common/db_obj/work_time.cpp \
    ../../libcommon/net/datagram.cpp \
    ../common/vc_err_code.cpp \
    ../../libcommon/net/net_cmd_client.cpp \
    ../../libcommon/net/net_lib.cpp \
    storages.cpp

HEADERS += \
    ../../libcommon/obj_db5/base.hpp \
    ../../libcommon/otl4/otlv4.h \
    ../../libcommon/thread/clthread.hpp \
    ../../libcommon/htmlreport/htmlrep.hpp \
    ../../libcommon/log2/log_file_writer.hpp \
    ../../libcommon/log2/log.hpp \
    ../../libcommon/system/system_env.hpp \
    ../../libcommon/system/text_help.hpp \
    ../../libcommon/net/net_server.h \
    ../../libcommon/net/net_http_server.h \
    ../../libcommon/system/time.hpp \
    ../../libcommon/net/net_http.h \
    ../../libcommon/net/net_server_tcp.h \
    ../../libcommon/net/net_lib_error.h \
    ../../libcommon/net/net_line_tcp.h \
    ../../libcommon/system/codepage.hpp \
    ../../libcommon/system/error.hpp \
    ../../libcommon/xml/ut_xml_tag.hpp \
    ../../libcommon/xml/ut_xml_par.hpp \
    vcall_station.hpp \
    sys_stat_holder.hpp \
    net_data_cserver.hpp \
    net_control_cserver.hpp \
    gl_server.hpp \
    ../common/version.hpp \
    ../common/user.hpp \
    ../common/stream_datagram.hpp \
    ../common/settings.hpp \
    ../common/net_packet.hpp \
    ../common/net_input.hpp \
    ../common/net_control_client.hpp \
    ../common/console.hpp \
    ../common/colors.hpp \
    ../common/chanel_map.hpp \
    ../common/db_obj/workstation.hpp \
    ../common/db_obj/user.hpp \
    ../common/db_obj/terminal.hpp \
    ../common/db_obj/server.hpp \
    ../common/db_obj/constant.hpp \
    ../common/db_obj/call.hpp \
    ../common/db_obj/queue.hpp \
    ../../libcommon/system/config_parser.hpp \
    ../common/db_obj/storage.hpp \
    ../../libcommon/obj_db5/base_db2.hpp \
    ../common/db_obj/work_time.hpp \
    ../../libcommon/net/datagram.hpp \
    ../common/vc_err_code.hpp \
    ../../libcommon/net/net_cmd_client.hpp \
    ../../libcommon/net/net_lib.h \
    storages.hpp

win32: LIBS += C:/DevTools/Qt/5.0.2/Tools/MinGW/i686-w64-mingw32/lib/libws2_32.a \
               D:/projects/vc/oci/lib/liboci.a \
               -luser32 -lshell32 -lgdi32 -lole32 -ladvapi32 -lodbc32

unix: LIBS += /lib/librt.so.1 \
              /lib/libpthread.so.0 \
              /opt/ibm/db2/V9.7/lib32/libdb2.so
#              /opt/oracle/product/11.2.0/client_1/lib/libclntsh.so.11.1

#release:LIBS += ../../libcommon/htmlreport/lib/htmlrep_lib.a
#debug:LIBS += ../../libcommon/htmlreport/lib/htmlrep_libd.a

OTHER_FILES += \
    qserver.set \
    ../common/imp.def \
    ../common/global.set \
    ../common/builds.txt \
    ../common/auth_frm.rc
