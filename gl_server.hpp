#ifndef __CIBERVIEW_SERVER_GLOBALS_H__
#define __CIBERVIEW_SERVER_GLOBALS_H__

#include <net_cmd_client.hpp>
#include "net_data_cserver.hpp"
#include "sys_stat_holder.hpp"
#include "../common/user.hpp"
#include <clthread.hpp>
#include <net_server.h>
#include "vcall_station.hpp"
#include "storages.hpp"
#include <base.hpp>

class _server_status
{
    public:

      struct _state
      {
          bool server_is_on;
          _state(bool s) : server_is_on(s) {};
      };

    private:

      static bool _need_exit;
      static bool _need_restart;

      static _net_cmd_client *net_cc;
      static std::string main_server;

      static std::string config_f_name;

      static _net_data_server *_data_server;

      static _state s_state;
      static _locker s_lock;

      static _vcall_station *vcs;

      static _db *db;
      static _db_id s_db_id;

      static _storages *cm_storages;

      //static _sys_stat_holder *_sys_stat;

    public:

      bool need_exit() { return _need_exit; };
      void need_exit(bool s) { _need_exit=s; };

      bool need_restart() { return _need_restart; };
      void need_restart(bool s) { _need_restart=s; };

      const std::string &main_server_name() const { return main_server; };
      void main_server_name(const std::string &s) { main_server = s; };

      _net_cmd_client *net_control() { return net_cc; };
      void net_control(_net_cmd_client *s) { net_cc=s; };

      const std::string &config_file_name() const { return config_f_name; };
      void config_file_name(const std::string &f) { config_f_name = f; };

      _net_data_server *data_server() { return _data_server; };
      void data_server(_net_data_server *s) { _data_server=s; };

      _vcall_station *call_station() { return vcs; };
      void call_station(_vcall_station *s) { vcs=s; };

      _storages *storages() { return cm_storages; };
      void storages(_storages *s) { cm_storages=s; };


      const _state state() const { s_lock.lock(); _state res=s_state; s_lock.unlock(); return res; };
      void state(const _state &s) { s_lock.lock(); s_state=s; s_lock.unlock(); };

      _db *database() { return db; };
      void database(_db *s) { db=s; };

      const _db_id &self_db_id() const { return s_db_id; };
      void self_db_id(const _db_id &s) { s_db_id=s; };

//      _sys_stat_holder *sys_stat() { return _sys_stat; };
//      void sys_stat(_sys_stat_holder *h) { _sys_stat=h; };

};



#endif

