#ifndef __VC_STORAGES_H__
#define __VC_STORAGES_H__

#include <string>
#include <list>
#include <time.hpp>
#include <base.hpp>

using namespace std;

typedef unsigned long long int64;

// ------------------------------------------------------------
class _storages
{
    protected:
        virtual ~_storages() {};

    public:

        struct _index
        {
            _db_id storage_id;
            int n;
        };

        _storages() {};

        virtual void release() = 0;

        virtual void init(const _db_id &server_id) = 0;
        virtual void shutdown() = 0;
        virtual bool is_inited() const = 0;

        virtual _index alloc_file(const _db_id &ref_id,const std::string &mime_type,int64 max_file_size,std::string &file_name,const std::string &file_ext) = 0;
        virtual void commit_file(const _index &alloc_index,int64 file_len) = 0;
};

// ------------------------------------------------------------
_storages *create_storages();//_storages::_process_notify_fun fun,int min_persent,int max_persent);

#endif
