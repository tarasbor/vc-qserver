#include "sys_stat_holder.hpp"
#include "../common/settings.hpp"

class _ss_holder:public _sys_stat_holder
{
    private:
    public:

       virtual void init();
       virtual void shutdown();
};

// -------------------------------------------
void _ss_holder::init()
{
    
}

// -------------------------------------------
void _ss_holder::shutdown()
{
}


// -------------------------------------------
_sys_stat_holder *create_sys_stat_holder()
{
    return new _ss_holder;
}
