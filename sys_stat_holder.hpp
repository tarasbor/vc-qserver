#ifndef __CYBERVIEW_SYSTEM_STAT_HOLDER__
#define __CYBERVIEW_SYSTEM_STAT_HOLDER__

class _sys_stat_holder
{
    private:
    public:

      virtual void init() = 0;
      virtual void shutdown() = 0;
      
};

_sys_stat_holder *create_sys_stat_holder();

#endif
