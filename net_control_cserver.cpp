#include "net_control_cserver.hpp"
#include "../common/user.hpp"
#include "../common/settings.hpp"
#include "../common/vc_err_code.hpp"
#include <net_server_tcp.h>
#include <map>
#include "gl_server.hpp"
#include <log.hpp>
#include <text_help.hpp>
#include <time.hpp>
#include <cstdlib>

#define MLOG(A) _log() << _log::_set_module_level("net_control_server",(A))

// ---------------------------------------------------
// some HELP functions
// ---------------------------------------------------
void make_err_rez(string &rez,int code,const string &comments)
{
	char tmp[32];
	rez="-err:";
	sprintf(tmp,"%i",code);
	rez+=tmp;
	rez+=":";
	rez+=_vc_err::_server_answ[code];
	rez+=":";
	rez+=comments;
	rez+=";\r\n";
}


// ---------------------------------------------------
// ---------------------------------------------------
class _net_control_server:public _net_server
{

    friend _net_server *create_net_control_server();

  private:

        _net_server_processor *processor;
        _net_server_process *listen;

        _net_control_server():listen(NULL),processor(NULL) {};
        virtual ~_net_control_server() { shutdown(); };

  public:

        virtual void online(const _net_adress &adress);
        virtual void shutdown();
        virtual void release() { delete this; };

        virtual bool in_process(std::string &in,std::string &out,_net_server_connection *nc);
        virtual void set_processor(_net_server_processor *pr) { processor=pr; };
		virtual _net_server_processor *get_processor() { return processor; };

        virtual void reg_connection(_net_server_connection *c);
        virtual void unreg_connection(_net_server_connection *c);

        virtual void keep_alive_s(int sec) { if (listen) listen->keep_alive_s(sec); };
        virtual int keep_alive_s() const { if (listen) return listen->keep_alive_s(); return 0; };

        virtual void reinit_time_s(int sec) { if (listen) listen->reinit_time_s(sec); };
        virtual int reinit_time_s() const { if (listen) return listen->reinit_time_s(); return 0; };
};

// ---------------------------------------------------
void _net_control_server::shutdown()
{
  if (!listen) return;
  listen->Kill();
  listen->release();
  listen=NULL;
}

// ---------------------------------------------------
void _net_control_server::online(const _net_adress &adress)
{
  if (listen) return;
  listen=create_tcp_server_process(adress,this);
  listen->Start();
  while(!listen->is_online()) { Sleep(100); };
  listen->keep_alive_s(0);
  listen->reinit_time_s(60*60);
}

// ---------------------------------------------------
bool _net_control_server::in_process(std::string &in,std::string &out,_net_server_connection *nc)
{
    if (!processor) return false;

    int pos=in.find(";\r\n");
    if (pos==std::string::npos) return false;

    bool res=processor->process(in,out,nc);
    if (res) in="";
    return res;
}

// ---------------------------------------------------
void _net_control_server::reg_connection(_net_server_connection *c)
{

}

// ---------------------------------------------------
void _net_control_server::unreg_connection(_net_server_connection *c)
{
    std::string act;
    const _net_adress *tcp_addr = c->net_line()->remoute_adress();

    try
    {
        if (_server_status().call_station())
        {
            act="Try drop call";
            if (_server_status().call_station()->drop_call(c))
            {
                MLOG(0) << "Drop call done - call connection from " << tcp_addr->get(0) << ":" << tcp_addr->get(1) << " was down..." << endl;
            }

            act="Try unreg WS";
            if (_server_status().call_station()->unreg_ws(c))
            {
                MLOG(0) << "Unreg WS done - connection from " << tcp_addr->get(0) << ":" << tcp_addr->get(1) << " was down..." << endl;
            }
        }
    }
    catch(_error &e)
    {
        MLOG(0) << "_net_control_server::unreg_connection " << act << " from " << tcp_addr->get(0) << ":" << tcp_addr->get(1) << " - error exception rise : " << e.str() << " : " << e.num() << endl;
    }
    catch(_nerror &e)
    {
        MLOG(0) << "_net_control_server::unreg_connection " << act << " from " << tcp_addr->get(0) << ":" << tcp_addr->get(1) << " - nerror exception rise : " << e.err_st() << " : " << e.err_no() << endl;
    }
}

// ---------------------------------------------------
_net_server *create_net_control_server() { return new _net_control_server(); }


// ----------------------------------------------------------------
// ----------------------------------------------------------------
class _control_processor:public _net_server_processor
{
    protected:

        typedef map<std::string,_user_info> _connections_map;
        _connections_map connections_map;

        virtual bool process(std::string &req,std::string &res,_net_server_connection *nc);

        void login_cmd(std::string &req,std::string &res,_net_line *nl);
        void reg_ws_cmd(std::string &req,std::string &res,_net_server_connection *nsc);
        void call_cmd(std::string &req,std::string &res,_net_server_connection *nsc);
        void call_answer_cmd(std::string &req,std::string &res,_net_server_connection *nsc);
        void call_stream_cmd(std::string &req,std::string &res,_net_server_connection *nsc);
        void call_end_cmd(std::string &req,std::string &res,_net_server_connection *nsc);
        void call_restore_cmd(std::string &req,std::string &res,_net_server_connection *nsc);

        void queue_get_size_cmd(std::string &req,std::string &res,_net_server_connection *nsc);

        virtual void reg_connection(_net_server_connection *c) {};
        virtual void unreg_connection(_net_server_connection *c) {};

    public:

        _control_processor() {};
        virtual ~_control_processor() {};
};

// ----------------------------------------------------------------
bool _control_processor::process(std::string &req, std::string &res, _net_server_connection *nc)
{

    MLOG(20) << req << endl;

    int pos = req.find(' ');
    if(pos == std::string::npos) pos = req.find(';');
    if(pos == std::string::npos)
    {
        make_err_rez(res,_vc_err::proto_unknown_format,"Ignore");
        //res = "-err: Unknown format! Ignore;\r\n";
        return true;
    }

    res = "-err: I don`t know this command;\r\n";

    std::string cmd = std::string(req, 0, pos);

    if(!_server_status().state().server_is_on && cmd != "get_server_run_status")
    {
        make_err_rez(res,_vc_err::common_wrong_state,"Server is not started yet");
        return true;
    }

    const _net_adress *tcp_addr = nc->net_line()->remoute_adress();

    try
    {

        if(cmd == "login") login_cmd(req, res, nc->net_line());
        else if(cmd == "get_server_run_status") res = "+ok: 101 online;\r\n";
        else
        {
            _connections_map::iterator cit = connections_map.find(tcp_addr->get(0) + ':' + tcp_addr->get(1));
            if(cit == connections_map.end())
            {
                make_err_rez(res,_vc_err::proto_login_first,"");
                return true;
            }
            else if(cmd == "shutdown")
            {
                res="+ok: I will shutdown...;\r\n";
                MLOG(0) << "I recive command to shutdown from " << (*cit).second.name() << " ..." << endl;
                _server_status().need_exit(true);
            }
            else if(cmd == "exit")
            {
                _connections_map::iterator it = connections_map.find(tcp_addr->get(0)+':'+tcp_addr->get(1));
                if (it!=connections_map.end())
                {
                    connections_map.erase(it);
                    res="+ok: Exit done...;\r\n";
                }
                else
                {
                    make_err_rez(res,_vc_err::proto_exit_fail,"Can not find connected user from your address (?)");
                }
            }
            else if(cmd == "reg_ws") reg_ws_cmd(req, res, nc);
            else if(cmd == "call") call_cmd(req, res, nc);
            else if(cmd == "call_restore") call_restore_cmd(req, res, nc);
            else if(cmd == "call_stream") call_stream_cmd(req, res, nc);
            else if(cmd == "call_end") call_end_cmd(req, res, nc);
            else if(cmd == "call_answer") call_answer_cmd(req, res, nc);
            else if(cmd == "queue_get_size") queue_get_size_cmd(req, res, nc);
        }
    }
    catch(_error &e)
    {
        MLOG(0) << "control_processor::process req=" << req << " from " << tcp_addr->get(0) << ":" << tcp_addr->get(1)
                << " - error exception rise : " << e.str() << " : " << e.num() << endl;
    }
    catch(_nerror &e)
    {
        MLOG(0) << "control_processor::process req=" << req << " from " << tcp_addr->get(0) << ":" << tcp_addr->get(1)
                << " - nerror exception rise : " << e.err_st() << " : " << e.err_no() << endl;
    }

    return true;
}

// ----------------------------------------------------------------
void _control_processor::login_cmd(std::string &req,std::string &res,_net_line *nl)
{

    std::string u_name=_text_help::get_field_from_st(req," \r;",1);
    std::string u_pass=_text_help::get_field_from_st(req," \r;",2);

    if (u_name=="" || u_pass=="")
    {
        make_err_rez(res,_vc_err::proto_incorrect_params,"Try again :-);");
        return;
    }

    const _net_adress *tcp_addr = nl->remoute_adress();

    MLOG(0) << "Login as " << u_name << " from " << tcp_addr->get(0) << ":" << tcp_addr->get(1) << " ... send it to main server..." << endl;

    std::string cmd="try_login ";
    cmd+=u_name;
    cmd+=" ";
    cmd+=u_pass;
    _server_status().net_control()->cmd(cmd,res);

    if (res[0]=='-')
    {
        make_err_rez(res,_vc_err::proto_login_fail,"Bad user or pass. Try again :-);");
        return;
    }

    MLOG(0) << "Login done..." << endl;

    _user_info u;
    u.name(u_name);

    connections_map[tcp_addr->get(0)+':'+tcp_addr->get(1)]=u;
}

// ----------------------------------------------------------------
void _control_processor::reg_ws_cmd(std::string &req,std::string &res,_net_server_connection *nsc)
{
    std::string ws_dns_name=_text_help::get_field_from_st(req," \r;",1);

    if (ws_dns_name.empty())
    {
        make_err_rez(res,_vc_err::proto_incorrect_params,"You must specify ws dns name.");
        return;
    }

    MLOG(0) << "Reg WS with dns name " << ws_dns_name << endl;

    if(_server_status().call_station()->reg_ws(ws_dns_name,nsc))
    {
        res="+ok: Registred to accept calls;\r\n";
        MLOG(0) << "Reg WS with dns name " << ws_dns_name << "...OK" << endl;
    }
    else
    {
        make_err_rez(res,_vc_err::common_wrong_state,"Can not reg you to accept calls");
        MLOG(0) << "Reg WS with dns name " << ws_dns_name << "...ERROR" << endl;
    }


}


// ----------------------------------------------------------------
void _control_processor::call_cmd(std::string &req,std::string &res,_net_server_connection *nsc)
{

    std::string caller=_text_help::get_field_from_st(req," \r;",1);
    std::string qname=_text_help::get_field_from_st(req," \r;",2);

    if (caller.empty() || qname.empty())
    {
        make_err_rez(res,_vc_err::proto_incorrect_params,"You must specify caller dns_name & queue name");
        return;
    }

    const _net_adress *tcp_addr = nsc->net_line()->remoute_adress();

    MLOG(0) << "Call from " << caller << " (" << tcp_addr->get(0) << ":" << tcp_addr->get(1) << ") to " << qname << endl;

    _vc_err::_code err;
    const _call *call = _server_status().call_station()->income_call(caller,qname,nsc,err);
    if (!call)
    {
        MLOG(0) << "Call REJECT: Code = " << err << endl;
        make_err_rez(res,err,"");
        return;
    }

    char tmp[128];
    sprintf(tmp,"+ok: call_id=%u;\r\n",call->id);
    res=tmp;

    sprintf(tmp,"Call ACCEPT: call_id=%u",call->id);
    MLOG(0) << tmp << endl;
}

// ----------------------------------------------------------------
void _control_processor::call_answer_cmd(std::string &req,std::string &res,_net_server_connection *nsc)
{

    std::string answer_from=_text_help::get_field_from_st(req," \r;",1);
    std::string qname=_text_help::get_field_from_st(req," \r;",2);

    if (answer_from.empty() || qname.empty())
    {
        make_err_rez(res,_vc_err::proto_incorrect_params,"You must specify answer ws dns name & queue name");
        return;
    }

    const _net_adress *tcp_addr = nsc->net_line()->remoute_adress();
    _connections_map::iterator cit = connections_map.find(tcp_addr->get(0)+':'+tcp_addr->get(1));

    MLOG(0) << "Answer call - ws=" << answer_from << " (" << tcp_addr->get(0) << ":" << tcp_addr->get(1)
            << ") from " << qname << " by user (operator) " << (*cit).second.name() << endl;

    const _call *cinfo = _server_status().call_station()->answer_for(answer_from,qname,(*cit).second.name(),nsc);
    if (!cinfo)
    {
        MLOG(0) << "Answer call REJECT: No such income queue or queue is empty..." << endl;
        res="-err: No such income queue;\r\n";
        return;
    }

    char tmp[256];
    sprintf(tmp,"+ok: call_id=%u from=%s video_rotate_angle=%i;\r\n",cinfo->id,cinfo->from_term.dns_name.c_str(),cinfo->video_rotate_angle);
    res=tmp;

    sprintf(tmp,"Answer call ACCEPT: call_id=%u (%s->%s,operator=%s)",cinfo->id,cinfo->from_term.dns_name.c_str(),answer_from.c_str(),(*cit).second.name().c_str());
    MLOG(0) << tmp << endl;
}


// ----------------------------------------------------------------
void _control_processor::call_end_cmd(std::string &req,std::string &res,_net_server_connection *nsc)
{

    string call_id_str=_text_help::get_field_from_st(req," \r;",1);
    unsigned int call_id=atoi(call_id_str.c_str());

    if (call_id==0)
    {
        make_err_rez(res,_vc_err::proto_incorrect_params,"You must specify call id");
        return;
    }

    MLOG(0) << "End call with id=" << call_id_str << endl;

    _server_status().call_station()->end_call(call_id,nsc);

    MLOG(0) << "End call with id=" << call_id_str << " done." << endl;


    res="+ok: Call ended;\r\n";
}

// ----------------------------------------------------------------
void _control_processor::call_restore_cmd(std::string &req,std::string &res,_net_server_connection *nsc)
{
    make_err_rez(res,_vc_err::proto_unknown_cmd,"call_restore not support yet");
    //res="-err: cmd call_restore not support yet;\r\n";
    /*
    std::string call_id=_text_help::get_field_from_st(req," \r;",1);

    if (call_id=="")
    {
        res="-err: Incorrect args. Try again :-);\r\n";
        return;
    }

    const _net_adress *tcp_addr = nl->remoute_adress();

    _log().out(std::string("Call restore from ")+tcp_addr->get(0)+":"+tcp_addr->get(1)+") with call_id = "+call_id,0,0);

    res="+ok: call_id=500;\r\n";
    */
}

// ----------------------------------------------------------------
void _control_processor::queue_get_size_cmd(std::string &req,std::string &res,_net_server_connection *nsc)
{

    std::string ws_dns_name=_text_help::get_field_from_st(req," \r;",1);
    std::string qname = _text_help::get_field_from_st(req, " \r;", 2);

    if(qname.empty() || ws_dns_name.empty())
    {
        make_err_rez(res,_vc_err::proto_incorrect_params,"You must specify ws dns name & queue name");
        //res="-err: Incorrect args. You must specify queue. Try again :-);\r\n";
        return;
    }

    int for_ws;
    int all;

    _server_status().call_station()->queue_get_size(qname,ws_dns_name,&for_ws,&all);

    char tmp[128];
    sprintf(tmp,"+ok: size for ws=%i all=%i;\r\n",for_ws,all);
    res=tmp;
}

// ----------------------------------------------------------------
void _control_processor::call_stream_cmd(std::string &req,std::string &res,_net_server_connection *nsc)
{
    std::string action      = _text_help::get_field_from_st(req, " \r;", 1);
    std::string call_id_str = _text_help::get_field_from_st(req, " \r;", 2);
    int call_id             = atoi(call_id_str.c_str());
    unsigned int data_ch_id;
    sscanf(_text_help::get_field_from_st(req, " \r;", 3).c_str(), "%u", &data_ch_id);

    char tmp[256];
    sprintf(tmp, "_control_processor::call_stream_cmd - %s %s %u %X", action.c_str(), call_id_str.c_str(), call_id, data_ch_id);
    MLOG(0) << tmp << endl;

    if(action == "bind" && data_ch_id == 0)
    {
        MLOG(0) << "WARNING!!! _control_processor::call_stream_cmd - bind - no data_ch_id" << endl;
        make_err_rez(res,_vc_err::proto_incorrect_params,"You must specify data channel id");
        return;
    }
    if(call_id == 0)
    {
        MLOG(0) << "WARNING!!! _control_processor::call_stream_cmd - bind/unbind - no call id" << endl;
        make_err_rez(res,_vc_err::proto_incorrect_params,"You must specify call id");
        return;
    }

    if(action == "bind")
    {
        // we try bind two streams (caller & answerer)
        // all work do call station (it alsow call net_data_server to bind two connections)
        if (_server_status().call_station()->bind_data_stream_to_call(call_id, data_ch_id, nsc))
            res="+ok: Binded;\r\n";
        else
            make_err_rez(res,_vc_err::common_wrong_state,"Can not bind data stream to this control connection.");
    }
    else if(action == "unbind")
    {
        // we try UN bind two streams (caller & answerer)
        // all work do call station (it alsow call net_data_server to UN bind two connections)
        if (_server_status().call_station()->unbind_data_stream_to_call(call_id, nsc))
            res="+ok: UNBinded;\r\n";
        else
            make_err_rez(res,_vc_err::common_wrong_state,"Can not UN bind data stream to this control connection.");
    }
    else make_err_rez(res,_vc_err::proto_incorrect_params,"Unknown action.");
}

// ----------------------------------------------------------------
_net_server_processor *create_net_control_processor()
{
    return new _control_processor();
};


