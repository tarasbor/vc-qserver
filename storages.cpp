#include <set>
#include "storages.hpp"
#include <error.hpp>
#include <clthread.hpp>
#include <log.hpp>
#include <text_help.hpp>
#include <udefs.hpp>

#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "../common/db_obj/storage.hpp"

#ifdef VC_DB2
    #include <base_db2.hpp>
    #define CREATE_DB   create_db_db2()
#endif
#ifdef VC_ORACLE
    #include <base_oracle.hpp>
    #define CREATE_DB   create_db_oracle()
#endif

#include <unistd.h>

#define MLOG(A) _log() << _log::_set_module_level("storage",(A))

// --------------------------------------------------------------------
// --------------------------------------------------------------------

// ------------------------------------------------------------
class _st
{
    protected:

        _db_id cm_id;
        string cm_path;
        string cm_mime_types;

        bool cm_accept_any;

        int64       cm_max_size_b;
        int64       cm_cur_size_b;

        int cm_cur_number;

        struct _file_rec
        {
            std::string name;
            _time create_time;
            int64 size;
        };
        typedef list<_file_rec> _files;
        _files cm_files;

        struct _alloc_rec
        {
            string ffname;      // f(ull) f(ile) name
            string fname;       // (f)ile name
            _time create_time;
            int64 size;
            int number;
        };
        typedef list<_alloc_rec> _alloc_list;
        _alloc_list cm_alloc_list;


        void load_dir();

    public:
        _st(_db_cl::_storage *st_db);
        virtual ~_st();

        inline const _db_id &id() const { return cm_id; }

        inline bool accept(const string &mtype) const;
        inline bool enough(int64 fsize) const { if (cm_cur_size_b+fsize<cm_max_size_b) return true; return false; }

        inline _time oldest_file_time() const;
        void remove_oldest_file();

        int alloc_file(const _db_id &ref_id,int64 max_file_size,std::string &file_name,const std::string &file_ext);
        void commit_file(int number,int64 file_len);
};

// ------------------------------------------------------------
_st::_st(_db_cl::_storage *st_db):cm_id(st_db->id()),cm_path(st_db->path()),
                                  cm_mime_types(st_db->mtypes()),cm_accept_any(false),
                                  cm_max_size_b(st_db->max_size()),cm_cur_size_b(0),
                                  cm_cur_number(1)
{
    if (cm_path[cm_path.length()-1]!=PATH_SLASH) cm_path.append(1,PATH_SLASH);

    MLOG(0) << "Init storage" << cm_path << endl;

    // in Mb - make in bytes
    cm_max_size_b*=1024*1024;

    if (cm_mime_types=="*") cm_accept_any=true;

    // load dir map & calc current size
    load_dir();

    MLOG(0) << "Init storage" << cm_path << " done:" << endl;
    MLOG(0) << "  Mime types   = " << cm_mime_types << endl;
    MLOG(0) << "  Files cout   = " << cm_files.size() << endl;
    MLOG(0) << "  Current size = " << cm_cur_size_b << endl;
    MLOG(0) << "  Max     size = " << cm_max_size_b << endl;
    MLOG(0) << "  Full (%)     = " << cm_cur_size_b*100/cm_max_size_b << endl;
}

// ------------------------------------------------------------
_st::~_st()
{
}

// ------------------------------------------------------------
void _st::load_dir()
{
    cm_cur_size_b=0;
    cm_files.clear();

    #ifdef WINDOWS
    #else

    DIR *d=opendir(cm_path.c_str());
    if (!d)
    {
        MLOG(0) << "Error: Storage load - can not open dir (" << cm_path << "). This storage will be off." << endl;
        cm_max_size_b=0;
        return;
    }

    dirent *de;
    struct stat stat_buf;
    while((de=readdir(d))!=NULL)
    {
        if (de->d_type!=DT_REG) continue;    // we use only regular files

        int res=stat((cm_path+de->d_name).c_str(),&stat_buf);
        if (res!=0)
        {
            MLOG(0) << "Error: Storage load - can not get file status (" << de->d_name << "). Skeep it but storage summary may be incorrect." << endl;
            continue;
        }

        _file_rec rec;
        rec.name=de->d_name;
        rec.size=stat_buf.st_size;
        rec.create_time.str_to_stamp(de->d_name);

        cm_files.push_back(rec);
        cm_cur_size_b+=rec.size;
    }

    closedir(d);

    #endif
}

// ------------------------------------------------------------
bool _st::accept(const string &mtype) const
{
    if (cm_accept_any || cm_mime_types.find(mtype)!=std::string::npos) return true;
    return false;
}

// ------------------------------------------------------------
_time _st::oldest_file_time() const
{
    if (cm_files.empty())
    {
        _time t;
        t.set_now();
        return t;
    }
    _files::const_iterator it=cm_files.begin();
    return it->create_time;
}

// ------------------------------------------------------------
void _st::remove_oldest_file()
{
    if (cm_files.empty()) return;

    _files::const_iterator it=cm_files.begin();

    cm_cur_size_b-=it->size;

    // remove
    // : from disk
    unlink((cm_path+it->name).c_str());
    // : from mem
    cm_files.pop_front();
}


// ------------------------------------------------------------
int _st::alloc_file(const _db_id &ref_id,int64 max_file_size,std::string &file_name,const std::string &file_ext)
{
    cm_cur_number++;
    cm_cur_size_b+=max_file_size;

    _alloc_rec ar;

    ar.number=cm_cur_number;
    ar.size=max_file_size;
    ar.create_time.set_now();

    char tmp_s[128];

    ar.fname=ar.create_time.stamp_to_str(tmp_s);
    ar.fname.append(1,'.');
    ar.fname+=file_ext;

    file_name=cm_path;
    file_name+=ar.fname;

    ar.ffname=file_name;

    cm_alloc_list.push_back(ar);

    return ar.number;
}

// ------------------------------------------------------------
void _st::commit_file(int number,int64 file_len)
{
    _alloc_list::iterator ait=cm_alloc_list.begin();
    while(ait!=cm_alloc_list.end())
    {
        if (ait->number==number) break;
        ait++;
    }

    if (ait==cm_alloc_list.end())
    {
        MLOG(0) << "Error. Storage commit file - no alloc_rec with given number." << endl;
        // MAY BE throw error
        return;
    }

    cm_cur_size_b-=ait->size;
    cm_cur_size_b+=file_len;

    _file_rec fr;
    fr.name=ait->fname;
    fr.create_time=ait->create_time;
    fr.size=file_len;

    cm_files.push_back(fr);
    cm_alloc_list.erase(ait);
}

// ------------------------------------------------------------
// ------------------------------------------------------------
class _sts:public _storages
{
    protected:

        bool cm_is_inited;
        _locker cm_lock;

        virtual ~_sts();

        typedef map<_db_id,_st *> _sts_map;
        _sts_map cm_sts_map;

    public:
        _sts();

        virtual void release() { delete this; };

        virtual void init(const _db_id &server_id);
        virtual void shutdown();
        virtual bool is_inited() const { return cm_is_inited; };

        virtual _storages::_index alloc_file(const _db_id &ref_id,const std::string &mime_type,int64 max_file_size,std::string &file_name,const std::string &file_ext);
        virtual void commit_file(const _storages::_index &alloc_index,int64 file_len);
};

// ------------------------------------------------------------
// ------------------------------------------------------------

// ------------------------------------------------------------
_sts::_sts():cm_is_inited(false)
{
}

// ------------------------------------------------------------
_sts::~_sts()
{
    shutdown();
}

// ------------------------------------------------------------
void _sts::init(const _db_id &server_id)
{
    MLOG(0) << "Init storages..." << endl;
    if (cm_is_inited) throw _error(0,"_sts::init - storages are already inited");

    cm_lock.lock();

    _db *db=NULL;
    _transaction *tr=NULL;

    try
    {
        db=CREATE_DB;
        tr=db->transaction();

        _db_cl::_storages_list::_for_qserver con(server_id);
        _db_cl::_storages_list::iterator *sit=_db_cl::_storages_list().begin(tr,&con);
        while(!sit->eof())
        {
            _st *st=new _st((*sit));
            cm_sts_map[(*sit)->id()]=st;

            (*sit)++;
        }
        delete sit;

        tr->commit();
        DEL(tr);
        DEL(db);
    }
    catch(_db_error &e)
    {
        MLOG(0) << "_sts::init - db error: " << e.error_st() << endl;

        if (tr) { try { DEL(tr); } catch(...) {} }
        if (db) { try { DEL(db); } catch(...) {} }

        cm_lock.unlock();
        return ;
    }

    cm_lock.unlock();

    cm_is_inited=true;
    MLOG(0) << "Init storages...done" << endl;
}

// ------------------------------------------------------------
void _sts::shutdown()
{
    if (!cm_is_inited) throw _error(0,"_sts::init - storages was not inited");

    cm_lock.lock();

    _sts_map::iterator it=cm_sts_map.begin();
    while(it!=cm_sts_map.end())
    {
        delete it->second;
        it++;
    }
    cm_sts_map.clear();

    cm_lock.unlock();


    cm_is_inited=false;
}

// ------------------------------------------------------------
_storages::_index _sts::alloc_file(const _db_id &ref_id,const std::string &mime_type,int64 max_file_size,std::string &file_name,const std::string &file_ext)
{
    if (!cm_is_inited) throw _error(0,"_sts::alloc_file - storages was not inited");

    _storages::_index rez;
    rez.storage_id=_db_id(0);
    rez.n=0;

    cm_lock.lock();

    list<_st *> l;

    _sts_map::iterator it=cm_sts_map.begin();
    while(it!=cm_sts_map.end())
    {
        if (it->second->accept(mime_type)) l.push_back(it->second);
        it++;
    }
    // now in l all storages, witch accept file mime type

    if (l.empty())
    {
        cm_lock.unlock();
        return rez;
    }

    list<_st *>::iterator lit;

    while(1)
    {

        lit=l.begin();
        while(lit!=l.end())
        {
            if ((*lit)->enough(max_file_size)) break; // find
            lit++;
        }

        if (lit!=l.end()) break; // find

        // there is no strorages with enouth space to store file
        // we need delete old files
        _time oldest_time;
        oldest_time.set_now();
        list<_st *>::iterator oldest_it=l.end();

        lit=l.begin();
        while(lit!=l.end())
        {
            _time ctime=(*lit)->oldest_file_time();
            if (ctime<oldest_time)
            {
                oldest_time=ctime;
                oldest_it=lit;
            }
            lit++;
        }

        if (oldest_it==l.end())   // no oldest file - ???
        {
            cm_lock.unlock();
            MLOG(0) << "Error. Storages alloc file - can not find oldest file in storages..." << endl;
            return rez;
        }

        (*oldest_it)->remove_oldest_file();
    }

    // now in lit storage ptr & this strorage ready to alloc file
    rez.storage_id=(*lit)->id();
    rez.n=(*lit)->alloc_file(ref_id,max_file_size,file_name,file_ext);

    cm_lock.unlock();

    return rez;
}

// ------------------------------------------------------------
void _sts::commit_file(const _storages::_index &alloc_index,int64 file_len)
{
    if (!cm_is_inited) throw _error(0,"_sts::commit_file - storages was not inited");

    cm_lock.lock();

    _sts_map::iterator it=cm_sts_map.find(alloc_index.storage_id);
    if (it==cm_sts_map.end())
    {
        cm_lock.unlock();
        MLOG(0) << "Error. Storage commit file - no such storage..." << endl;
        throw _error(0,"_sts::commit_file - can not find storage by id");
    }

    it->second->commit_file(alloc_index.n,file_len);

    cm_lock.unlock();
}

// ------------------------------------------------------------
// ------------------------------------------------------------
_storages *create_storages()
{
    return new _sts;
}
