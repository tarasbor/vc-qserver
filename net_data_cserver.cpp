#include "net_data_cserver.hpp"
#include <net_server_tcp.h>
#include <map>
#include "gl_server.hpp"
#include <log.hpp>
#include <error.hpp>

#include <datagram.hpp>
#include "../common/stream_datagram.hpp"

#include <cstring>

#define MLOG(A) _log() << _log::_set_module_level("net_data_server",(A))

// ---------------------------------------------------
// ---------------------------------------------------
class _net_data_server_imp:public _net_data_server
{

  friend _net_data_server *create_net_data_server();

  private:

        _net_server_process *listen;

        struct _con_info
        {
            _con_info(_net_server_connection *c):con(c),ref(1) {};

            _net_server_connection *con;
            unsigned int ref;

            void use() { ref++; };
            void free() { ref--; };
        };

        typedef map<int,_con_info *> _cons_map;
        _cons_map cm_cons_map;
        _locker cm_cons_map_lock;

        static unsigned int cm_con_ref;

        /*
        struct _chanel_pair
        {
            _net_server_connection *con;
            unsigned int binded_data_con_id;

            unsigned int call_id;

            _chanel_pair():con(NULL),binded_data_con_id(0),call_id(0) {};
        };

        typedef map<unsigned int,_chanel_pair> _cons_map;
        _cons_map cons_map;
        _locker cons_map_locker;

        int connection_id;
        */

        _net_data_server_imp():listen(NULL)/*,connection_id(1)*/ {};
        virtual ~_net_data_server_imp() { shutdown(); };

  public:

        virtual void online(const _net_adress &adress);
        virtual void shutdown();
        virtual void release() { delete this; };

        virtual bool in_process(std::string &in,std::string &out,_net_server_connection *nc);
        virtual void set_processor(_net_server_processor *pr) { };
		virtual _net_server_processor *get_processor() { return NULL; };

        virtual void reg_connection(_net_server_connection *c);
        virtual void unreg_connection(_net_server_connection *c);

        virtual _net_server_connection *get_con(unsigned int id);
        virtual void free_con(unsigned int id);

        //void bind_data_streams(unsigned int call_id,unsigned int first_id,unsigned int second_id);
        //void unbind_data_streams(unsigned int data_ch_id);

        virtual void keep_alive_s(int sec) { if (listen) listen->keep_alive_s(sec); };
        virtual int keep_alive_s() const { if (listen) return listen->keep_alive_s(); return 0; };

        virtual void reinit_time_s(int sec) { if (listen) listen->reinit_time_s(sec); };
        virtual int reinit_time_s() const { if (listen) return listen->reinit_time_s(); return 0; };
};

unsigned int _net_data_server_imp::cm_con_ref=1;

// ---------------------------------------------------
void _net_data_server_imp::shutdown()
{
    cm_cons_map_lock.lock();

    _cons_map::iterator cit=cm_cons_map.begin();
    while(cit!=cm_cons_map.end())
    {
        _con_info *ci=cit->second;
        cit->second=NULL;
        delete ci;
        cit++;
    }

    cm_cons_map.clear();

    cm_cons_map_lock.unlock();
    /*
    cons_map_locker.lock();
    _cons_map::iterator it = cons_map.begin();
    while(it!=cons_map.end())
    {
        //if ((*it).second.source) delete (*it).second.source;
        // WARNING !!! You do not need delete (*it).second.con - connection will be deleted themself
        it++;
    }
    cons_map.clear();
    cons_map_locker.unlock();
    */

    if (!listen) return;
    listen->Kill();
    listen->release();
    listen=NULL;
}

// ---------------------------------------------------
void _net_data_server_imp::online(const _net_adress &adress)
{
  if (listen) return;
  listen=create_tcp_server_process(adress,this);
  listen->Start();
  while(!listen->is_online()) { Sleep(100); };
  listen->keep_alive_s(0);
  listen->reinit_time_s(60*60);
}

// ---------------------------------------------------
bool _net_data_server_imp::in_process(std::string &in,std::string &out,_net_server_connection *c_in)
{
    cm_cons_map_lock.lock();

    // find id for this connection
    _cons_map::iterator cit=cm_cons_map.begin();
    while(cit!=cm_cons_map.end())
    {
        if (cit->second->con == c_in) break;
        cit++;
    }

    if (cit==cm_cons_map.end())
    {
        // no such REGISTRED connection
        cm_cons_map_lock.unlock();
        return true;
    }

    unsigned int in_id=cit->first;

    cm_cons_map_lock.unlock();

    // and say call station to process in data
    _server_status().call_station()->proc_in_data(in,in_id);

    return true;

    /*
    // At first we determinate second (out) data connection
    _net_server_connection *c_out = NULL;
    int call_id = 0;

    cons_map_locker.lock();

    try
    {

        _cons_map::iterator self_it = cons_map.find((unsigned int)c_in);
        if (self_it!=cons_map.end())
        {
            _cons_map::iterator binded_it = cons_map.find(self_it->second.binded_data_con_id);
            if (binded_it!=cons_map.end())
            {
                c_out = binded_it->second.con;
                call_id = binded_it->second.call_id;
            }
        }

        // WAS unlock here - it is not right (may be) second connection may down (& drop call & drop second size) while we parse & resend data;
        // cons_map_locker.unlock();

        // if out connection is binden & not null we:
        // 1. Extract stream datagrams from in data (if have one or more)
        // 2. Analize each datagram (and make some actions if need)
        // 3. Copy datagam (in data) to out data
        // ---------------
        // 4. Send out data (put it out buffer) use second (out) connection
        // if out conenction is NOT binded or = NULL we DROP (?) input data (may we wrong - may be we must save some in data - need think)
        if (c_out)
        {
            string out_data="";

            while(1)
            {
                _datagram *sd=NULL;

                if (in.size()>=_datagram::header_len())
                {
                    int s; // raw data size
                    memcpy(&s,in.data()+(_datagram::header_len()-sizeof(int)),sizeof(int));

                    if (in.size()>=s+_datagram::header_len())
                    {
                        sd=new _datagram(in.data());
                        _server_status().call_station()->proccess_stream_datagram_for_call(call_id,c_in,sd);
                        delete sd;
                        out_data+=string(in,0,s+_datagram::header_len());
                        in.erase(0,s+_datagram::header_len());
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            if (!out_data.empty())
            {
                while(!c_out->locker()->try_lock()) Sleep(1);
                c_out->to_out(out_data);
                c_out->locker()->unlock();
            }
        }
        else
        {
            if (in.size()>10*1024*1024)
            {
                char tmp[256];
                sprintf(tmp,"WARNING!!! Data connection (id=%u) have input buffer more then 10M (no out connection) - input buffer will be dropped...",(unsigned int)c_in);
                MLOG(0) << tmp << endl;
                in.clear();
            }
        }
    }
    catch(_error &e)
    {
        MLOG(0) << "_net_data_server_imp::in_process - _error exception rise " << e.str() << ":" << e.num() << endl;
    }
    catch(_nerror &e)
    {
        MLOG(0) << "_net_data_server_imp::in_process - _nerror exception rise " << e.err_st() << ":" << e.err_no() << endl;
    }

    cons_map_locker.unlock();
    */
}

// ---------------------------------------------------
void _net_data_server_imp::reg_connection(_net_server_connection *c)
{
    const _net_adress *ra=c->net_line()->remoute_adress();
    MLOG(15) << "Reg data connection from " << ra->get(0) << ":" << ra->get(1) << endl;

    // reg
    cm_cons_map_lock.lock();

    unsigned int con_id=cm_con_ref;
    cm_con_ref++;

    _con_info *ci=new _con_info(c);

    cm_cons_map[con_id]=ci;
    _cons_map::iterator cit=cm_cons_map.find(con_id);
    cit->second->use();

    cm_cons_map_lock.unlock();

    // send connection id to client
    unsigned char tmp[sizeof(unsigned int)];
    memcpy(tmp,&con_id,sizeof(unsigned int));
    _datagram sd(tmp,sizeof(unsigned int),_stream_datagram::connection_id);

    while(!c->locker()->try_lock()) Sleep(1);
    c->to_out(std::string((char *)sd.data(),sd.size()));
    c->locker()->unlock();

    cit->second->free();

    MLOG(15) << "Reg data connection from " << ra->get(0) << ":" << ra->get(1) << " done. Id = " << con_id << endl;

    /*
    cons_map_locker.lock();

    _chanel_pair p;
    p.con = c;
    unsigned int ptr=(unsigned int)c;

    cons_map[ptr]=p;

    unsigned char tmp[sizeof(int)];
    memcpy(tmp,&ptr,sizeof(int));
    _datagram sd(tmp,sizeof(int),_stream_datagram::connection_id);

    while(!c->locker()->try_lock()) Sleep(1);
    c->to_out(std::string((char *)sd.data(),sd.size()));
    c->locker()->unlock();

    char st[255];
    sprintf(st,"Reg id = %X",ptr);
    MLOG(15) << st << endl;

    cons_map_locker.unlock();
    */
}

// ---------------------------------------------------
void _net_data_server_imp::unreg_connection(_net_server_connection *c)
{
    const _net_adress *ra=c->net_line()->remoute_adress();
    MLOG(15) << "Unreg data connection from " << ra->get(0) << ":" << ra->get(1) << endl;

    cm_cons_map_lock.lock();

    _cons_map::iterator cit=cm_cons_map.begin();
    while(cit!=cm_cons_map.end())
    {
        if (cit->second->con==c) break;
        cit++;
    }

    if (cit==cm_cons_map.end())
    {
        MLOG(0) << "_net_data_server_imp::unreg_connection - warning - unreg data connection from "
                << ra->get(0) << ":" << ra->get(1) << ", but it is not in reg map." << endl;
        cm_cons_map_lock.unlock();
        return;
    }

    // we can not erase connection info from map
    // because it can be use now & when it will be free - we do not find connection in map & do not dec reference and block free mem code
    /*
    _con_info *ci=cit->second;
    cm_cons_map.erase(cit);
    */

    // we just set con ptr to NULL - no one can get it now
    cit->second->con=NULL;

    cm_cons_map_lock.unlock();

    // and we must wait end of usage
    bool first=true;
    while(cit->second->ref > 1)
    {
        if (first)
        {
            MLOG(0) << "Unreg data connection from " << ra->get(0) << ":" << ra->get(1) << " - wait while connection using end. Ref = " << cit->second->ref << endl;
            first=false;
        }
        Sleep(1);
    }

    // and now we can unreg connection
    cm_cons_map_lock.lock();
    delete cit->second;
    cm_cons_map.erase(cit);
    cm_cons_map_lock.unlock();

    MLOG(15) << "Unreg data connection from " << ra->get(0) << ":" << ra->get(1) << " done." << endl;
    /*
    cons_map_locker.lock();

    _cons_map::iterator it = cons_map.begin();
    while(it!=cons_map.end())
    {
        if ((*it).second.con==c)
        {
            if ((*it).second.binded_data_con_id)
            {
                _cons_map::iterator sit = cons_map.find((*it).second.binded_data_con_id);
                if (sit!=cons_map.end())
                {
                    sit->second.binded_data_con_id = 0;
                }
            }

            cons_map.erase(it);
            break;
        }
        it++;
    }
    cons_map_locker.unlock();
    MLOG(15) << "Unreg connection done..." << endl;
    */
}

// ---------------------------------------------------
_net_server_connection *_net_data_server_imp::get_con(unsigned int id)
{
    cm_cons_map_lock.lock();

    _cons_map::iterator cit=cm_cons_map.find(id);
    if (cit==cm_cons_map.end())
    {
        cm_cons_map_lock.unlock();
        return NULL;
    }

    // we use only if con ptr is valid
    if (cit->second->con) cit->second->use();

    cm_cons_map_lock.unlock();

    return cit->second->con;
}

// ---------------------------------------------------
void _net_data_server_imp::free_con(unsigned int id)
{
    cm_cons_map_lock.lock();

    _cons_map::iterator cit=cm_cons_map.find(id);
    if (cit==cm_cons_map.end())
    {
        cm_cons_map_lock.unlock();
        return;
    }

    // we ALWAYS free
    cit->second->free();

    cm_cons_map_lock.unlock();
}

/*
// ---------------------------------------------------
void _net_data_server_imp::bind_data_streams(unsigned int call_id,unsigned int first_id,unsigned int second_id)
{
    MLOG(10) << "data_server::bind_data_streams" << endl;

    cons_map_locker.lock();

    _cons_map::iterator first_it = cons_map.find(first_id);
    if (first_it==cons_map.end())
    {
        MLOG(0) << "WARNING!!! data_server::bind_data_streams - can not find data connection by first id" << endl;
        cons_map_locker.unlock();
        return;
    }

    _cons_map::iterator second_it = cons_map.find(second_id);
    if (second_it==cons_map.end())
    {
        MLOG(0) << "WARNING!!! data_server::bind_data_streams - can not find data connection by second id" << endl;
        cons_map_locker.unlock();
        return;
    }

    int n=0;
    _datagram sd((unsigned char *)&n,sizeof(int),_stream_datagram::second_side_bind);

    first_it->second.binded_data_con_id  = second_id;
    first_it->second.call_id=call_id;

    second_it->second.binded_data_con_id = first_it->first;
    second_it->second.call_id=call_id;

    // WE send bind only to first connection
    // Client on first side must send to second side bind datagram itself
    while(!first_it->second.con->locker()->try_lock()) Sleep(1);
    first_it->second.con->to_out(std::string((char *)sd.data(),sd.size()));
    first_it->second.con->locker()->unlock();

    MLOG(10) << "data_server::bind_data_streams done" << endl;

    cons_map_locker.unlock();
}

// ---------------------------------------------------
void _net_data_server_imp::unbind_data_streams(unsigned int data_ch_id)
{
    /*
    cons_map_locker.lock();

    _cons_map::iterator first_it = cons_map.find(data_ch_id);
    if (first_it==cons_map.end())
    {
        cons_map_locker.unlock();
        return;
    }

    int second_id=first_it->second.binded_data_con_id;
    first_it->second.binded_data_con_id = 0;

    _cons_map::iterator second_it = cons_map.find(second_id);
    if (second_it==cons_map.end())
    {
        cons_map_locker.unlock();
        return;
    }

    second_it->second.binded_data_con_id = 0;

    cons_map_locker.unlock();
}
*/

// ---------------------------------------------------
_net_data_server *create_net_data_server() { return new _net_data_server_imp(); }

