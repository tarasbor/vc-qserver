#include "gl_server.hpp"

bool _server_status::_need_exit = false;
bool _server_status::_need_restart = false;

std::string _server_status::config_f_name;

_net_data_server *_server_status::_data_server = NULL;

std::string _server_status::main_server;
_net_cmd_client *_server_status::net_cc;

_server_status::_state _server_status::s_state(false);
_locker _server_status::s_lock;

_vcall_station *_server_status::vcs = NULL;

_db *_server_status::db = NULL;
_db_id _server_status::s_db_id = 0;

_storages *_server_status::cm_storages = NULL;

//_sys_stat_holder *_server_status::_sys_stat = NULL;

