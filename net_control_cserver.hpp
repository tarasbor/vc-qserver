#ifndef __CIBERVIEW_SERVER_NETWORK_CONTROLL_H__
#define __CIBERVIEW_SERVER_NETWORK_CONTROLL_H__

#include <net_server.h>

_net_server *create_net_control_server();
_net_server_processor *create_net_control_processor();

#endif
