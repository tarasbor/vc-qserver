#ifndef __VIDEO_CALL_STATION_H__
#define __VIDEO_CALL_STATION_H__

#include <net_server.h>
#include <time.hpp>
#include <string>
#include <fstream>
#include <datagram.hpp>
#include "../common/db_obj/call.hpp"
#include "storages.hpp"
#include "../common/vc_err_code.hpp"

using namespace std;

#define END_CALL_CODE_NORMAL           0
#define END_CALL_CODE_USER_ABORTED     1
#define END_CALL_CODE_CRUSHED          2


struct _file_rec
{
    _storages::_index index;
    ofstream *f_ptr;
    string ffname;      // full file name

    _db_cl::_call_data::_doc_type type;
};

// ------------------------------------
struct _call_point
{
    string dns_name;
    _net_server_connection *con;

    unsigned int data_con_id;
    //_net_server_connection *data_con;

    _file_rec video_file;
    _file_rec audio_file;
};

// ------------------------------------
struct _ws_info
{
    string dns_name;
    _net_server_connection *connection;

    _time reg_at;
    _time last_call_end_at;
};

// ------------------------------------
struct _call
{
    int id;

    _time ring_time;
    _time answer_time;
    _time end_time;

    string in_q;

    _call_point from_term;
    _call_point answer_ws;

    string operator_login;

    int end_code;
    int printed_doc_cnt;
    int scaned_doc_cnt;

    int video_rotate_angle;

    _file_rec scan_file;
    _file_rec print_file;

    list<_file_rec> files;
};

class _vcall_station
{
    public:

        _vcall_station() {};
        virtual ~_vcall_station() {};

        virtual bool reg_ws(const string &ws_dns_name,_net_server_connection *c) = 0;
        virtual bool unreg_ws(_net_server_connection *c) = 0;

        virtual const _call *income_call(const string &term_dns_name,const string &qname,_net_server_connection *c,_vc_err::_code &rez) = 0;
        virtual const _call *answer_for(const string &ws_dns_name,const string &qname,const string &oper_login,_net_server_connection *c) = 0;

        virtual bool end_call(unsigned int call_id,_net_server_connection *c) = 0;
        virtual bool drop_call(_net_server_connection *c) = 0;

        virtual void queue_get_size(const string &qname,const string &ws_dns_name,int *for_ws,int *all) = 0;

        virtual unsigned int bind_data_stream_to_call(unsigned int call_id,unsigned int data_con_id,_net_server_connection *c) = 0;
        virtual unsigned int unbind_data_stream_to_call(unsigned int call_id,_net_server_connection *c) = 0;

        virtual void proc_in_data(std::string &in,unsigned int in_con_id) = 0;
        virtual void proccess_stream_datagram_for_call(unsigned int call_id,unsigned int in_con_id,const _datagram *sd) = 0;
};


//----------------------------------------------------
_vcall_station *create_vcall_station();

#endif
