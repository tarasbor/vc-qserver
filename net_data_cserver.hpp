#ifndef __CIBERVIEW_SERVER_NETWORK_DATA_H__
#define __CIBERVIEW_SERVER_NETWORK_DATA_H__

#include <net_server.h>

class _net_data_server:public _net_server
{
    public:

        virtual _net_server_connection *get_con(unsigned int id) = 0;
        virtual void free_con(unsigned int id) = 0;
      //virtual void bind_data_streams(unsigned int call_id,unsigned int first_id,unsigned int second_id)=0;
      //virtual void unbind_data_streams(unsigned int data_ch_id)=0;

};

_net_data_server *create_net_data_server();


#endif
